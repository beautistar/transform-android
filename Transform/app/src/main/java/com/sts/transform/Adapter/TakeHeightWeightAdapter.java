package com.sts.transform.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/19/2017.
 */

public class TakeHeightWeightAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> allData =  new ArrayList<>();

    public TakeHeightWeightAdapter(Context context) {
        this.context = context;
    }

    public void refresh(ArrayList<String> data){
        this.allData.clear();
        this.allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public String getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        CustomHolder holder;
        if (convertView ==  null){
            holder =  new CustomHolder();

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView =  inflater.inflate(R.layout.item_take_height, viewGroup, false);
            holder.setId(convertView);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        String height = allData.get(position);
        holder.setData(height);

        return convertView;
    }

    private class CustomHolder{
        TextView txvHeight;

        private void setId(View view){
            txvHeight = (TextView)view.findViewById(R.id.txvHeight);
        }

        private void setData(String height){
            txvHeight.setText(height);
        }
    }
}
