package com.sts.transform.Base;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by HGS on 12/11/2015.
 */

public abstract class BaseFragment  extends Fragment implements View.OnClickListener{

    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showCenterToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

    @Override
    public void onClick(View v) {

    }
}
