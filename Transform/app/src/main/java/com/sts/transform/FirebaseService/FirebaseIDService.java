package com.sts.transform.FirebaseService;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by STS on 5/25/2017.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Commons.token = refreshedToken;

        Preference.getInstance().put(this, PrefConst.FIREBASE_TOKEN, refreshedToken);
    }

}
