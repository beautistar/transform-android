package com.sts.transform.Models;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by STS on 12/20/2017.
 */

public class RankingModel implements Serializable{

    int userId = 0, totalPoints = 0;
    String userName = "", photoUrl = "";


    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
