package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.LibraryModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/9/2017.
 */

public class LibraryAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<LibraryModel> allData = new ArrayList<>();

    public LibraryAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<LibraryModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public LibraryModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomHolder holder;
        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_library, parent, false);
            holder.setId(convertView);

            LibraryModel model = allData.get(position);
            holder.setData(model);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class  CustomHolder{

        TextView txvTitle, txvDescription;
        ImageView imvPreviewImage;

        private void setId(View view){
            txvTitle = (TextView)view.findViewById(R.id.txvTitle);
            txvDescription =  (TextView)view.findViewById(R.id.txvDescription);
            imvPreviewImage = (ImageView)view.findViewById(R.id.imvPreviewImage);
        }

        private void setData(LibraryModel model){
            txvTitle.setText(model.getTitle());
            txvDescription.setText(model.getDescription());
            if (model.getImageUrl().length() > 0){
                Picasso.with(activity).load(model.getImageUrl()).into(imvPreviewImage);
            }
        }
    }
}
