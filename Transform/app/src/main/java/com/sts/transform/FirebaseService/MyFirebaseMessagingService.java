package com.sts.transform.FirebaseService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.login.CustomTabLoginMethodHandler;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.PointModel;
import com.sts.transform.R;

import java.util.Map;

/**
 * Created by STS on 5/25/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private Vibrator _vibrator;

    Context _context;

    String messageType = "", content = "", body = "", title = "";
    String orderId = "";
    static int cnt = 1;
    private static final String COMMON = "com.sts.chat";

    public LocalBroadcastManager lbm;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        this._context = getApplicationContext();

        Map<String, String> data = remoteMessage.getData();

        title = remoteMessage.getNotification().getTitle();
        body = remoteMessage.getNotification().getBody();

        messageType = data.get("msgType");
        content = data.get("content");

        if (content.equals("you received benchmark point")){
            lbm = LocalBroadcastManager.getInstance(this);
            lbm.sendBroadcast(new Intent(Constants.BENCHMARK_POINT));
        }

        checkPush();
    }

    private void checkPush() {
        if (messageType ==  null) return;
        if (messageType.equals(Constants.CHAT_MESSAGE)){

            Intent intent = new Intent();
            intent.setAction(COMMON);
            intent.putExtra(Constants.CHAT_MODEL, content);
            sendBroadcast(intent);
            showPushNotification();
        }else {
            showPushNotification();
        }
    }

    private void showPushNotification() {

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(_context, MainActivity.class);
        intent.putExtra("Title", title);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final NotificationManager notificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(_context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(_context);
        builder
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(title)
                .setSubText(body)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(contentIntent)
                .setSound(alarmSound)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(cnt, notification);
    }
}
