package com.sts.transform.Utils;

import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;

/**
 * Created by STS on 2/26/2018.
 */

public class PointSaveUtils extends BaseActivity{

    private void saveCheckedPoint(String checkedPoint) {
        switch (checkedPoint){
            case Constants.pointContent1:
                Preference.getInstance().put(getApplication(), PrefConst.POINT1, "1");
                break;
            case Constants.pointContent2:
                Preference.getInstance().put(getApplication(), PrefConst.POINT2, "1");
                break;
            case Constants.pointContent3:
                Preference.getInstance().put(getApplication(), PrefConst.POINT3, "1");
                break;
            case Constants.pointContent4:
                Preference.getInstance().put(getApplication(), PrefConst.POINT4, "1");
                break;
            case Constants.pointContent5:
                Preference.getInstance().put(getApplication(), PrefConst.POINT5, "1");
                break;
            case Constants.pointContent6:
                Preference.getInstance().put(getApplication(), PrefConst.POINT6, "1");
                break;
            case Constants.pointContent7:
                Preference.getInstance().put(getApplication(), PrefConst.POINT7, "1");
                break;
            case Constants.pointContent8:
                Preference.getInstance().put(getApplication(), PrefConst.POINT8, "1");
                break;
            case Constants.pointContent9:
                Preference.getInstance().put(getApplication(), PrefConst.POINT9, "1");
                break;
            case Constants.pointContent10:
                Preference.getInstance().put(getApplication(), PrefConst.POINT10, "1");
                break;
            case Constants.pointContent11:
                Preference.getInstance().put(getApplication(), PrefConst.POINT11, "1");
                break;
            case Constants.pointContent12:
                Preference.getInstance().put(getApplication(), PrefConst.POINT12, "1");
                break;
            case Constants.pointContent13:
                Preference.getInstance().put(getApplication(), PrefConst.POINT13, "1");
                break;
            case Constants.pointContent14:
                Preference.getInstance().put(getApplication(), PrefConst.POINT14, "1");
                break;
            case Constants.pointContent15:
                Preference.getInstance().put(getApplication(), PrefConst.POINT15, "1");
                break;
        }
    }

    private void removeCheckedPoint(String checkedPoint) {
        switch (checkedPoint){
            case Constants.pointContent1:
                Preference.getInstance().put(getApplication(), PrefConst.POINT1, "0");
                break;
            case Constants.pointContent2:
                Preference.getInstance().put(getApplication(), PrefConst.POINT2, "0");
                break;
            case Constants.pointContent3:
                Preference.getInstance().put(getApplication(), PrefConst.POINT3, "0");
                break;
            case Constants.pointContent4:
                Preference.getInstance().put(getApplication(), PrefConst.POINT4, "0");
                break;
            case Constants.pointContent5:
                Preference.getInstance().put(getApplication(), PrefConst.POINT5, "0");
                break;
            case Constants.pointContent6:
                Preference.getInstance().put(getApplication(), PrefConst.POINT6, "0");
                break;
            case Constants.pointContent7:
                Preference.getInstance().put(getApplication(), PrefConst.POINT7, "0");
                break;
            case Constants.pointContent8:
                Preference.getInstance().put(getApplication(), PrefConst.POINT8, "0");
                break;
            case Constants.pointContent9:
                Preference.getInstance().put(getApplication(), PrefConst.POINT9, "0");
                break;
            case Constants.pointContent10:
                Preference.getInstance().put(getApplication(), PrefConst.POINT10, "0");
                break;
            case Constants.pointContent11:
                Preference.getInstance().put(getApplication(), PrefConst.POINT11, "0");
                break;
            case Constants.pointContent12:
                Preference.getInstance().put(getApplication(), PrefConst.POINT12, "0");
                break;
            case Constants.pointContent13:
                Preference.getInstance().put(getApplication(), PrefConst.POINT13, "0");
                break;
            case Constants.pointContent14:
                Preference.getInstance().put(getApplication(), PrefConst.POINT14, "0");
                break;
            case Constants.pointContent15:
                Preference.getInstance().put(getApplication(), PrefConst.POINT15, "0");
                break;
        }
    }
}
