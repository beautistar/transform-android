package com.sts.transform.Main;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class SplashActivity extends BaseActivity {

    String dd = "";

    String[][] bodyFatPercentTableForMale = new String [38][58];
    String[][] abcTableForFemale = new String [62][6];
    ArrayList<String> heightList = new ArrayList<>();
    ArrayList<String> weightList = new ArrayList<>();
    ArrayList<String> hipList =  new ArrayList<>();
    ArrayList<String> abdomenList = new ArrayList<>();
    ArrayList<String> wristList = new ArrayList<>();
    ArrayList<String> waistList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getBodyFatFromExcelFile();

        getABCFromExcelFile();

        gotoLoginActivity();

        getHeightData();
        getWeightData();
        getHipData();
        getAbdomenData();
        getWristData();
        getWaistData();

//        showNoti();

        initial();
    }

    private void initial() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        dd = df.format(c);

        dd = (dd.split("-"))[0];

        String savedDate = Preference.getInstance().getValue(this, PrefConst.DATE_FOR_POINT, "");
        if (!savedDate.equals(dd)){
            Preference.getInstance().put(this, PrefConst.POINT1, "0");
            Preference.getInstance().put(this, PrefConst.POINT2, "0");
            Preference.getInstance().put(this, PrefConst.POINT3, "0");
            Preference.getInstance().put(this, PrefConst.POINT4, "0");
            Preference.getInstance().put(this, PrefConst.POINT5, "0");
            Preference.getInstance().put(this, PrefConst.POINT6, "0");
            Preference.getInstance().put(this, PrefConst.POINT7, "0");
            Preference.getInstance().put(this, PrefConst.POINT8, "0");
            Preference.getInstance().put(this, PrefConst.POINT9, "0");
            Preference.getInstance().put(this, PrefConst.POINT10, "0");
            Preference.getInstance().put(this, PrefConst.POINT11, "0");
            Preference.getInstance().put(this, PrefConst.POINT12, "0");
            Preference.getInstance().put(this, PrefConst.POINT13, "0");
            Preference.getInstance().put(this, PrefConst.POINT14, "0");
            Preference.getInstance().put(this, PrefConst.POINT15, "0");
        }

        String savedDateForBlock = Preference.getInstance().getValue(this, PrefConst.DATE_FOR_BLOCK, "");
        if (!savedDateForBlock.equals(dd)){
            Preference.getInstance().put(this, PrefConst.eatBlockTillNow, 0);
        }
    }

    private void getABCFromExcelFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                readABCForFemaleFromExcelFile();
            }
        }).start();
    }

    private void getHeightData() {
        heightList = new ArrayList<>();
        for (float height = 55.0f; height < 76.5f; height += 0.5f){
            String strHeight = String.valueOf(height);
            heightList.add(strHeight);
        }

        Commons.heightList = heightList;
    }

    private void getWeightData() {
        weightList =  new ArrayList<>();
        for (int weight = 120; weight < 305; weight += 5){
            String strWeight = String.valueOf(weight);
            weightList.add(strWeight);
        }

        Commons.weightList = weightList;
    }

    private void getHipData() {
        hipList = new ArrayList<>();
        for (float hip = 30.0f; hip < 60.5f; hip += 0.5f){
            String strHip = String.valueOf(hip);
            hipList.add(strHip);
        }

        Commons.hipList = hipList;
    }

    private void getAbdomenData() {

        abdomenList = new ArrayList<>();
        for (float abdomen = 20.0f; abdomen < 50.5f; abdomen +=0.5f){
            String strAbdomen = String.valueOf(abdomen);
            abdomenList.add(strAbdomen);
        }

        Commons.abdomenList = abdomenList;
    }

    private void getWristData() {
        wristList = new ArrayList<>();
        for (float wrist =3.5f; wrist < 11.5f; wrist += 0.5f){
            String strWrist = String .valueOf(wrist);
            wristList.add(strWrist);
        }

        Commons.wristList = wristList;
    }

    private void getWaistData() {
        waistList = new ArrayList<>();
        for (float waist = 20.f;  waist < 60.5f; waist ++){
            String strWaist = String.valueOf(waist);
            waistList.add(strWaist);
        }

        Commons.waistList = waistList;
    }

    private void readABCForFemaleFromExcelFile() {

        try {
            // Creating Input Stream

            InputStream myInput;

            //  Don't forget to Change to your assets folder excel sheet
            myInput = getAssets().open("female_a_b_c_table.xlsx");

            XSSFWorkbook workbook = new XSSFWorkbook(myInput);
            XSSFSheet mySheet = workbook.getSheetAt(0);

            /** We now need something to iterate through the cells. **/
            Iterator<Row> rowIter = mySheet.rowIterator();


            int i = 0;
            while (rowIter.hasNext()) {
                Row myRow = (Row) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();

                int j = 0;
                while (cellIter.hasNext()) {
                    XSSFCell myCell = (XSSFCell) cellIter.next();
                    abcTableForFemale[i][j] = "";
                    abcTableForFemale[i][j] = myCell.toString();
                    j++;
                }
                i++;
            }

        } catch (Exception e) {
            // e.printStackTrace();
        }

        Commons.abcTableForFemale = abcTableForFemale;

        return;
    }

    private void gotoLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                closeProgress();
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
    private void getBodyFatFromExcelFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                readBodyFatForMaleFromExcelFile();
            }
        }).start();
    }

    private void readBodyFatForMaleFromExcelFile() {
        try {
            // Creating Input Stream

            InputStream myInput;

            //  Don't forget to Change to your assets folder excel sheet
            myInput = getAssets().open("male_body_fat_table.xlsx");

            XSSFWorkbook workbook = new XSSFWorkbook(myInput);
            XSSFSheet mySheet = workbook.getSheetAt(0);

            /** We now need something to iterate through the cells. **/
            Iterator<Row> rowIter = mySheet.rowIterator();


            int i = 0;
            while (rowIter.hasNext()) {
                Row myRow = (Row) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();

                int j = 0;
                while (cellIter.hasNext()) {
                    XSSFCell myCell = (XSSFCell) cellIter.next();
                    bodyFatPercentTableForMale[i][j] = "";
                    bodyFatPercentTableForMale[i][j] = myCell.toString();
                    j++;
                }
                i++;
            }

        } catch (Exception e) {
            // e.printStackTrace();
        }

        Commons.bodyFatPercentTableForMale = bodyFatPercentTableForMale;

        return;
    }

    private void showNoti(){

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(_context, MainActivity.class);
        intent.putExtra("Title", "aaaaa");
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final NotificationManager notificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(_context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(_context);
        builder
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("bbbbb")
                .setSubText("cccc")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(contentIntent)
                .setSound(alarmSound)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
    }
}
