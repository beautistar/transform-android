package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/26/2017.
 */

public class GroupMemberListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<UserModel> allData = new ArrayList<>();

    public GroupMemberListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<UserModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public UserModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView == null){

            holder = new CustomHolder();

            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_benchmark, parent, false);

            holder.setId(convertView);

            UserModel model = allData.get(position);
            holder.setData(model);

            convertView.setTag(holder);
        }else{
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class CustomHolder{
        TextView txvUserName, txvTotalPoints;
        ImageView imvCheck, imvPhoto;

        private void setId (View view){
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            imvCheck = (ImageView)view.findViewById(R.id.imvCheck);
            imvPhoto = (ImageView)view.findViewById(R.id.imvPhoto);
            txvTotalPoints = (TextView)view.findViewById(R.id.txvTotalPoints);
        }

        private void setData(UserModel model){

            if (model.getPhotoUrl().length() > 0){
                Picasso.with(activity).load(model.getPhotoUrl()).into(imvPhoto);
            }

            txvUserName.setText(model.getUserName());
            txvTotalPoints.setText(String.valueOf(model.getTotalPointOfSelectedGroup()));

            if (model.getBenchmark() == 1){
                imvCheck.setVisibility(View.VISIBLE);
            }else {
                imvCheck.setVisibility(View.INVISIBLE);
            }
        }
    }
}
