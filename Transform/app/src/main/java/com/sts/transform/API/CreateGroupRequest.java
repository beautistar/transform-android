package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class CreateGroupRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_CREATE_GROUP;
    private Map<String,String> params;

    public CreateGroupRequest(String userId, String groupName, String kindOfGroup, String groupType, String pointParameter, String price, String creatorName, String startDate, String endDate, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
        params.put(ReqConst.GROUP_NAME, groupName);
        params.put(ReqConst.KIND_OF_GROUP, kindOfGroup);
        params.put(ReqConst.GROUP_TYPE, groupType);
        params.put(ReqConst.POINT_PARAMETER, pointParameter);
        params.put(ReqConst.PRICE, price);
        params.put(ReqConst.CREATOR_NAME, creatorName);
        params.put(ReqConst.START_DATE, startDate);
        params.put(ReqConst.END_DATE, endDate);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
