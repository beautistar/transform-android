package com.sts.transform.Main;

import android.animation.IntEvaluator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Chat.ChatFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Fragment.CreateGroupFragment;
import com.sts.transform.Fragment.EditProfileFragment;
import com.sts.transform.Fragment.HomeFragment;
import com.sts.transform.Fragment.HowFragment;
import com.sts.transform.Fragment.InvitedGroupInfoFragment;
import com.sts.transform.Fragment.JoinGroupFragment;
import com.sts.transform.Fragment.LibraryFragment;
import com.sts.transform.Fragment.ManageAccountFragment;
import com.sts.transform.Fragment.ProfileFragment;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.STRef;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    HomeFragment mHomeFragment;
    HowFragment mHowFragment;
    LibraryFragment mLibraryFragment;
    ProfileFragment mProfileFragment;
    EditProfileFragment mEditFragment;
    CreateGroupFragment mCreateGroupFragment;
    JoinGroupFragment mJoinGroupFragment;
    ManageAccountFragment mManageAccountFragment;
    InvitedGroupInfoFragment mInvitedGroupInfoFragment;
    ChatFragment mChatFragment;

    FrameLayout frmContainer;
    DrawerLayout dlContainer;
    View menu;
    ActionBarDrawerToggle drawerToggle ;
    NavigationView navMenu;
    ImageView imvMenu;
    Button btnHome, btnHow, btnLibrary, btnProfile, btnBack, btnCreateGroup, btnManageAccount, btnLogout;
    TextView txvJoinGroup;
    public TextView txvInviteBadge;

    public boolean completedProfile =  true;

    MyReceiver  receiver;
    private LocalBroadcastManager lbm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /// init receiver
        lbm = LocalBroadcastManager.getInstance(this);
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.BENCHMARK_POINT);
        lbm.registerReceiver(receiver, filter);

        loadLayout();
    }

    private void loadLayout() {
        dlContainer = (DrawerLayout)findViewById(R.id.dlContainer);
        dlContainer.addDrawerListener(drawerToggle);

        navMenu = (NavigationView)findViewById(R.id.navMenu);
        navMenu.setNavigationItemSelectedListener(this);
        menu = navMenu.getHeaderView(0);

        ImageView imgFacebook = (ImageView) menu.findViewById(R.id.img_facebook);
        imgFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fbIntent = new Intent("android.intent.action.VIEW", Uri.parse(Constants.facebookURL));
                startActivity(fbIntent);
            }
        });

        ImageView imgInstagram = (ImageView) menu.findViewById(R.id.img_instagram);
        imgInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent instaIntent = new Intent("android.intent.action.VIEW", Uri.parse(Constants.instagramURL));
                startActivity(instaIntent);
            }
        });

        btnHome = (Button)menu.findViewById(R.id.btnHome);
        btnHome.setOnClickListener(this);

        btnHow = (Button)menu.findViewById(R.id.btnHow);
        btnHow.setOnClickListener(this);

        btnLibrary = (Button)menu.findViewById(R.id.btnLibrary);
        btnLibrary.setOnClickListener(this);

        btnProfile = (Button)menu.findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(this);

        imvMenu = (ImageView)findViewById(R.id.imvMenu);
        imvMenu.setOnClickListener(this);

        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        txvJoinGroup = (TextView) menu.findViewById(R.id.txvJoinGroup);
        txvJoinGroup.setOnClickListener(this);

        btnCreateGroup = (Button)menu.findViewById(R.id.btnCreateGroup);
        btnCreateGroup.setOnClickListener(this);

        btnManageAccount = (Button)menu.findViewById(R.id.btnManageAccount);
        btnManageAccount.setOnClickListener(this);

        btnLogout = (Button)menu.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

        frmContainer = (FrameLayout)findViewById(R.id.frmContainer);

        checkProfile();

        txvInviteBadge = (TextView)findViewById(R.id.txvInviteBadge);

        gotoHomeFragment();
    }

    public void checkProfile(){
        if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE) && (Commons.user.getUserPropertyModel().getAverageWrist().equals("0.00") ||
                Commons.user.getUserPropertyModel().getAverageWrist().equals("")) && (Commons.user.getUserPropertyModel().getAverageWaist().equals("0.00")||
                Commons.user.getUserPropertyModel().getAverageWaist().equals(""))){
            completedProfile = false;
        }

        if (Commons.user.getUserPropertyModel().getGender().equals(Constants.FEMALE) && (Commons.user.getUserPropertyModel().getAverageHip().equals("0.00") ||
                Commons.user.getUserPropertyModel().getAverageHip().equals("")) && (Commons.user.getUserPropertyModel().getAverageAbdomen().equals("0.00") ||
                Commons.user.getUserPropertyModel().getAverageAbdomen().equals(""))){
            completedProfile = false;
        }
    }

    public void gotoHomeFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mHomeFragment = new HomeFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mHomeFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.HOME_FRAGMENT;
    }

    private void gotoHowFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mHowFragment = new HowFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mHowFragment);
        fragmentTransaction.commit();

        Commons.currentFragment =  Constants.HOW_FRAGMENT;
    }

    private void gotoLibraryFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mLibraryFragment = new LibraryFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mLibraryFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.LIBRARY_FRAGMENT;
    }

    public void gotoProfileFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mProfileFragment = new ProfileFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mProfileFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.PROFILE_FRAGMENT;
    }

    private void gotoCreateGroupFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mCreateGroupFragment = new CreateGroupFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mCreateGroupFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.CREATE_GROUP_FRAGMENT;
    }

    public void gotoEditFragment(){
        imvMenu.setVisibility(View.GONE);
        btnBack.setVisibility(View.VISIBLE);

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mEditFragment = new EditProfileFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mEditFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.EDIT_PROFILE_FRAGMENT;
    }

    public void gotoJoinGroupFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mJoinGroupFragment = new JoinGroupFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mJoinGroupFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.JOIN_GROUP_FRAGMENT;
    }

    public void gotoManageAccountFragment(){
        imvMenu.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.GONE);
        dlContainer.closeDrawers();

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mManageAccountFragment = new ManageAccountFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mManageAccountFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.MANAGE_ACCOUNT_FRAGMENT;
    }

    public void gotoInvitedGroupInfoFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mInvitedGroupInfoFragment = new InvitedGroupInfoFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mInvitedGroupInfoFragment);
        fragmentTransaction.commit();

        Commons.currentFragment = Constants.INVITED_GROUP_INFO_FRAGMENT;
    }

    public void gotoChatFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mChatFragment = new ChatFragment(this);
        fragmentTransaction.replace(R.id.frmContainer, mChatFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    private void logout() {
        Preference.getInstance().put(this, PrefConst.EMAIL, "");
        Preference.getInstance().put(this, PrefConst.PASSWORD, "");

        Preference.getInstance().put(this, PrefConst.POINT1, "0");
        Preference.getInstance().put(this, PrefConst.POINT2, "0");
        Preference.getInstance().put(this, PrefConst.POINT3, "0");
        Preference.getInstance().put(this, PrefConst.POINT4, "0");
        Preference.getInstance().put(this, PrefConst.POINT5, "0");
        Preference.getInstance().put(this, PrefConst.POINT6, "0");
        Preference.getInstance().put(this, PrefConst.POINT7, "0");
        Preference.getInstance().put(this, PrefConst.POINT8, "0");
        Preference.getInstance().put(this, PrefConst.POINT9, "0");
        Preference.getInstance().put(this, PrefConst.POINT10, "0");
        Preference.getInstance().put(this, PrefConst.POINT11, "0");
        Preference.getInstance().put(this, PrefConst.POINT12, "0");
        Preference.getInstance().put(this, PrefConst.POINT13, "0");
        Preference.getInstance().put(this, PrefConst.POINT14, "0");
        Preference.getInstance().put(this, PrefConst.POINT15, "0");

        Preference.getInstance().put(this, PrefConst.eatBlockTillNow, 0);
        Preference.getInstance().put(this, PrefConst.DATE_FOR_BLOCK, "");
        Preference.getInstance().put(this, PrefConst.DATE_FOR_POINT, "");
        Preference.getInstance().put(this, PrefConst.SELECTED_GROUP_ID, 0);

        Commons.rankingModelList.clear();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvMenu:
                dlContainer.openDrawer(Gravity.LEFT);
                break;
            case R.id.btnHome:
                gotoHomeFragment();
                break;
            case R.id.btnHow:
                gotoHowFragment();
                break;
            case R.id.btnLibrary:
                gotoLibraryFragment();
                break;
            case R.id.btnProfile:
                gotoProfileFragment();
                break;
            case R.id.btnBack:
                gotoProfileFragment();
                break;
            case R.id.btnCreateGroup:
                gotoCreateGroupFragment();
                break;
            case R.id.txvJoinGroup:
                gotoJoinGroupFragment();
                break;
            case R.id.btnManageAccount:
                gotoManageAccountFragment();
                break;
            case R.id.btnLogout:
                logout();
                break;
        }
    }

    class MyReceiver extends BroadcastReceiver {
        MyReceiver() {}
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case Constants.BENCHMARK_POINT:
                    refreshUserInfo();
                    if (Commons.currentFragment == Constants.HOME_FRAGMENT){
                        String todayPoint =  mHomeFragment.txvTodayPoints.getText().toString().trim();
                        String totalPoint =  mHomeFragment.txvTotalPoints.getText().toString().trim();

                        int intTodayPoint = 0, intTotalPoint = 0;
                        if (todayPoint.length() > 0 && totalPoint.length() > 0){
                            intTodayPoint = Integer.valueOf(todayPoint) + 5;
                            intTotalPoint = Integer.valueOf(totalPoint) + 5;

                            mHomeFragment.txvTodayPoints.setText(String.valueOf(intTodayPoint));
                            mHomeFragment.txvTotalPoints.setText(String.valueOf(intTotalPoint));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
