package com.sts.transform.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sts.transform.Chat.ChatFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;

public class MyReceiver extends BroadcastReceiver {

    public MyReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Commons.isChatFragment){

            String messageBody = intent.getStringExtra(Constants.CHAT_MODEL);
            ChatFragment.instance.getInstance().receiveMessage(messageBody);
        }

/*        String messageBody = intent.getStringExtra(Constants.CHAT_MODEL);
        Log.d("aaaaa", messageBody + "+");*/
    }
}
