package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.CreateGroupRequest;
import com.sts.transform.API.GetUserInfoByNameRequest;
import com.sts.transform.API.JoinGroupRequest;
import com.sts.transform.API.SendPushForInviteRequest;
import com.sts.transform.Adapter.AutoCompleteTextViewAdapter;
import com.sts.transform.Adapter.MemberListAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.MemberModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class CreateGroupFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener{

    private final String START_DATE = "startDate";
    private final String END_DATE = "endDate";

    MainActivity activity;
    MemberListAdapter mMemberListAdapter;

    AutoCompleteTextViewAdapter mAutoCompleteTextViewAdapter;
    AutoCompleteTextViewAdapter mAutoCompleteTextViewAdapter_1;

    int index = 0;

    LinearLayout lytContainer;
    RelativeLayout rytContainer;
    ImageView imvPrev, imvNext;
    EditText edtGroupName, edtPrice, edtName, edtEmail, edtPhoneNumber;
    TextView txvPrivate1, txvPrivate2, txvPublic1, txvPublic2, txvGeneralTitle, txvGeneralContent, txvMacroTitle, txvMacroContent, txvStartDate, txvEndDate;
    TextView txvPaleoTitle, txvPaleoContent, txvKetoTitle, txvKetoContent;
    LinearLayout lytPublic, lytPrivate, lytName;
    Button btnSubmit, btnAddMember;
    ProgressBar pbFindUserName, pbFindUserEmail;
    ListView lstUserInfo, lstEmailList;
    CheckBox chPoint1, chPoint2, chPoint3, chPoint4, chPoint5, chPoint6, chPoint7, chPoint8, chPoint9, chPoint10, chPoint11, chPoint12;
    ListView lstMemberList;

    LinearLayout lytContainer1, lytContainer2;

    String groupName = "", kindOfGroup = "", groupType = "", pointParameters = "", price = "", beforeName = "", startDate = "", endDate = "", beforeEmail = "";
    ArrayList<View> childViewList = new ArrayList<>();
    RelativeLayout.LayoutParams params;
    UserModel invitedUser;
    int groupId = 0, year = 0, month = 0, day = 0;
    String startOrEndDate = "";
    int blackColor, redColor, invitedUserCnt = 0;
    String memberName = "", memberEmail = "", memberPhoneNumber = "";

    Calendar calendar;;

    @SuppressLint("ServiceCast")
    public CreateGroupFragment(MainActivity activity) {

        this.activity = activity;

        blackColor = activity.getResources().getColor(R.color.black);
        redColor = activity.getResources().getColor(R.color.red);

        params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);

        LayoutInflater inflater =  LayoutInflater.from(activity);
        View childView = inflater.inflate(R.layout.create_group_0, null, false);
        childViewList.add(childView);
        childView = inflater.inflate(R.layout.create_group_1, null, false);
        childViewList.add(childView);
        childView = inflater.inflate(R.layout.create_group_2, null, false);
        childViewList.add(childView);

        childView = inflater.inflate(R.layout.create_group_3, null, false);
        childViewList.add(childView);

        childView = inflater.inflate(R.layout.create_group_4, null, false);
        childViewList.add(childView);

        setViewId();

        getTodayDate();
    }

    private void getTodayDate() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void setViewId() {

        edtGroupName = (EditText)childViewList.get(0).findViewById(R.id.edtGroupName);
        txvPrivate1 = (TextView)childViewList.get(0).findViewById(R.id.txvPrivate1);
        txvPrivate2 = (TextView)childViewList.get(0).findViewById(R.id.txvPrivate2);
        txvPublic1 =  (TextView)childViewList.get(0).findViewById(R.id.txvPublic1);
        txvPublic2 =  (TextView)childViewList.get(0).findViewById(R.id.txvPublic2);

        txvStartDate = (TextView)childViewList.get(0).findViewById(R.id.txvStartDate);
        txvStartDate.setOnClickListener(this);

        txvEndDate = (TextView)childViewList.get(0).findViewById(R.id.txvEndDate);
        txvEndDate.setOnClickListener(this);

        lytPublic = (LinearLayout)childViewList.get(0).findViewById(R.id.lytPublic);
        lytPublic.setOnClickListener(this);

        lytPrivate = (LinearLayout)childViewList.get(0).findViewById(R.id.lytPrivate);
        lytPrivate.setOnClickListener(this);

        txvGeneralTitle =  (TextView)childViewList.get(1).findViewById(R.id.txvGeneralTitle);
        txvGeneralContent = (TextView)childViewList.get(1).findViewById(R.id.txvGeneralContent);
        txvGeneralContent.setOnClickListener(this);

        txvMacroTitle =  (TextView)childViewList.get(1).findViewById(R.id.txvMacroTitle);
        txvMacroContent = (TextView)childViewList.get(1).findViewById(R.id.txvMacroContent);
        txvMacroContent.setOnClickListener(this);

        chPoint1 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint1);
        chPoint2 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint2);
        chPoint3 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint3);
        chPoint4 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint4);
        chPoint5 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint5);

        chPoint6 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint6);
        chPoint6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chPoint6.isChecked()){
                    showBenchmarkAlert();
                }
            }
        });

        chPoint7 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint7);
        chPoint8 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint8);
        chPoint9 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint9);
        chPoint10 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint10);
        chPoint11 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint11);
        chPoint12 = (CheckBox)childViewList.get(2).findViewById(R.id.chPoint12);

        lytContainer1 = (LinearLayout)childViewList.get(2).findViewById(R.id.lytContainer1);
        lytContainer2 = (LinearLayout)childViewList.get(2).findViewById(R.id.lytContainer2);

        edtPrice = (EditText)childViewList.get(3).findViewById(R.id.edtPrice);

        btnSubmit = (Button) childViewList.get(4).findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        btnAddMember = (Button)childViewList.get(4).findViewById(R.id.btnAddMember);
        btnAddMember.setOnClickListener(this);

        lytName = (LinearLayout)childViewList.get(4).findViewById(R.id.lytName);
        edtName = (EditText)childViewList.get(4).findViewById(R.id.edtName); edtName.setOnClickListener(this);
        edtEmail = (EditText)childViewList.get(4).findViewById(R.id.edtEmail);
        edtPhoneNumber = (EditText)childViewList.get(4).findViewById(R.id.edtPhoneNumber);

        mAutoCompleteTextViewAdapter = new AutoCompleteTextViewAdapter(activity);
        lstUserInfo = (ListView)childViewList.get(4).findViewById(R.id.lstUserInfo);
        lstUserInfo.setAdapter(mAutoCompleteTextViewAdapter);
        lstUserInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                beforeName = mAutoCompleteTextViewAdapter.getAllData().get(position).getUserName();
                edtName.setText(beforeName);
                edtEmail.setText(mAutoCompleteTextViewAdapter.getAllData().get(position).getEmail());
                lstUserInfo.setVisibility(View.GONE);
                invitedUser = mAutoCompleteTextViewAdapter.getAllData().get(position);
            }
        });

        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

                String name = edtName.getText().toString().trim();

                if (beforeName.equals(name)) return;

                if (name.length() == 0) {
                    lstUserInfo.setVisibility(View.GONE);
                    pbFindUserName.setVisibility(View.GONE);
                }else {
                    searchUserInfoByName(name);
                }
            }
        });

        mAutoCompleteTextViewAdapter_1 = new AutoCompleteTextViewAdapter(activity);
        lstEmailList = (ListView)childViewList.get(4).findViewById(R.id.lstEmailList);
        lstEmailList.setAdapter(mAutoCompleteTextViewAdapter_1);
        lstEmailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                beforeEmail = mAutoCompleteTextViewAdapter_1.getAllData().get(position).getEmail();
                edtEmail.setText(beforeEmail);
                edtName.setText(mAutoCompleteTextViewAdapter_1.getAllData().get(position).getUserName());
                lstEmailList.setVisibility(View.GONE);
                invitedUser = mAutoCompleteTextViewAdapter_1.getAllData().get(position);
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {

                String email = edtEmail.getText().toString().trim();
                if (beforeEmail.equals(email)) return;

                if (email.length() == 0){
                    lstEmailList.setVisibility(View.GONE);
                    pbFindUserEmail.setVisibility(View.GONE);
                }else {
                    searchUserInfoByEmail(email);
                }
            }
        });

        pbFindUserName = (ProgressBar)childViewList.get(4).findViewById(R.id.pbFindUserName);
        pbFindUserName.setIndeterminate(false);
        pbFindUserEmail = (ProgressBar)childViewList.get(4).findViewById(R.id.pbFindUserEmail);
        pbFindUserEmail.setIndeterminate(false);

        txvPaleoTitle = (TextView)childViewList.get(1).findViewById(R.id.txvPaleoTitle);
        txvPaleoContent = (TextView)childViewList.get(1).findViewById(R.id.txvPaleoContent);
        txvPaleoContent.setOnClickListener(this);

        txvKetoTitle = (TextView)childViewList.get(1).findViewById(R.id.txvKetoTitle);
        txvKetoContent = (TextView)childViewList.get(1).findViewById(R.id.txvKetoContent);
        txvKetoContent.setOnClickListener(this);

        mMemberListAdapter = new MemberListAdapter(activity);
        lstMemberList = (ListView)childViewList.get(4).findViewById(R.id.lstMemberList);
        lstMemberList.setAdapter(mMemberListAdapter);
    }

    private void showBenchmarkAlert() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_benchmark);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button btnYes = (Button)dialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chPoint6.setChecked(true);
                dialog.dismiss();
            }
        });

        Button btnNo =  (Button)dialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chPoint6.setChecked(false);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void searchUserInfoByName(String userName) {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                pbFindUserName.setVisibility(View.GONE);
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<UserModel> mUserModelList = new ArrayList<>();
                        JSONArray jsonUserModelList = response.getJSONArray(ReqConst.USER_MODEL_LIST);

                        for (int i = 0; i < jsonUserModelList.length(); i ++){

                            JSONObject jsonUserModel = (JSONObject)jsonUserModelList.get(i);
                            UserModel  mUserModel = new UserModel();

                            mUserModel.setId(jsonUserModel.getInt(ReqConst.USER_ID));
                            mUserModel.setEmail(jsonUserModel.getString(ReqConst.EMAIL));
                            mUserModel.setUserName(jsonUserModel.getString(ReqConst.USER_NAME));

                            mUserModelList.add(mUserModel);
                        }

                        lstUserInfo.setVisibility(View.VISIBLE);
                        mAutoCompleteTextViewAdapter.refresh(mUserModelList);

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbFindUserName.setVisibility(View.GONE);
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        pbFindUserName.setVisibility(View.VISIBLE);
        GetUserInfoByNameRequest req = new GetUserInfoByNameRequest(userName, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void searchUserInfoByEmail(String email) {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                pbFindUserEmail.setVisibility(View.GONE);
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<UserModel> mUserModelList = new ArrayList<>();
                        JSONArray jsonUserModelList = response.getJSONArray(ReqConst.USER_MODEL_LIST);

                        for (int i = 0; i < jsonUserModelList.length(); i ++){

                            JSONObject jsonUserModel = (JSONObject)jsonUserModelList.get(i);
                            UserModel  mUserModel = new UserModel();

                            mUserModel.setId(jsonUserModel.getInt(ReqConst.USER_ID));
                            mUserModel.setEmail(jsonUserModel.getString(ReqConst.EMAIL));
                            mUserModel.setUserName(jsonUserModel.getString(ReqConst.USER_NAME));

                            mUserModelList.add(mUserModel);
                        }

                        lstUserInfo.setVisibility(View.VISIBLE);
                        mAutoCompleteTextViewAdapter.refresh(mUserModelList);

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbFindUserName.setVisibility(View.GONE);
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        pbFindUserEmail.setVisibility(View.VISIBLE);
        GetUserInfoByNameRequest req = new GetUserInfoByNameRequest(email, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_create_group, container, false);

        lytContainer = (LinearLayout)fragment.findViewById(R.id.lytContainer);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                lstUserInfo.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(lytContainer.getWindowToken(), 0);
                return false;
            }
        });

        rytContainer = (RelativeLayout)fragment.findViewById(R.id.rytContainer);

        imvNext = (ImageView)fragment.findViewById(R.id.imvNext);
        imvNext.setOnClickListener(this);

        imvPrev = (ImageView)fragment.findViewById(R.id.imvPrev);
        imvPrev.setOnClickListener(this);

        addChildView();

        return fragment;
    }

    private void addChildView() {
        if ((index > childViewList.size() - 1) || index < 0) return;

        imvPrev.setVisibility(View.VISIBLE); imvNext.setVisibility(View.VISIBLE);
        if (index == 0) {imvPrev.setVisibility(View.GONE); imvNext.setVisibility(View.VISIBLE);}
        if (index == childViewList.size() - 1){imvPrev.setVisibility(View.VISIBLE); imvNext.setVisibility(View.GONE);}

        rytContainer.removeAllViews();
        rytContainer.addView(childViewList.get(index), params);

        if (index == 2){
            lytContainer1.setVisibility(View.VISIBLE);
            if (groupType.equals(Constants.GROUP_GENERAL_TYPE)){
                chPoint1.setVisibility(View.GONE);
            }else {
                chPoint1.setVisibility(View.VISIBLE);
            }
        }

        if (index ==  4){
            createGroup();
        }
    }

    private void choicePrivate() {
        initCreateGroup0View();
        txvPrivate1.setTextColor(redColor);
        txvPrivate2.setTextColor(redColor);

        kindOfGroup = Constants.PRIVATE;
    }

    private void choicePublic() {
        initCreateGroup0View();
        txvPublic1.setTextColor(redColor);
        txvPublic2.setTextColor(redColor);

        kindOfGroup = Constants.PUBLIC;
    }

    private void initCreateGroup0View() {
        txvPublic1.setTextColor(blackColor);
        txvPublic2.setTextColor(blackColor);
        txvPrivate1.setTextColor(blackColor);
        txvPrivate2.setTextColor(blackColor);

        kindOfGroup = "";
    }

    private void choiceGeneral() {
        initCreateGroup1View();
        txvGeneralTitle.setTextColor(redColor);
        txvGeneralContent.setTextColor(redColor);

        groupType = Constants.GROUP_GENERAL_TYPE;
    }

    private void choiceMacroContent() {
        initCreateGroup1View();
        txvMacroTitle.setTextColor(redColor);
        txvMacroContent.setTextColor(redColor);

        groupType = Constants.GROUP_MACRO_TYPE;
    }

    private void choicePaleoMacroContent(){
        initCreateGroup1View();
        txvPaleoTitle.setTextColor(redColor);
        txvPaleoContent.setTextColor(redColor);

        groupType = Constants.GROUP_PALEO_TYPE;
    }

    private void choiceKetoContent() {
        initCreateGroup1View();
        txvKetoTitle.setTextColor(redColor);
        txvKetoContent.setTextColor(redColor);

        groupType = Constants.GROUP_KETO_TYPE;
    }

    private void initCreateGroup1View() {
        txvGeneralTitle.setTextColor(blackColor);
        txvGeneralContent.setTextColor(blackColor);
        txvMacroTitle.setTextColor(blackColor);
        txvMacroContent.setTextColor(blackColor);
        txvPaleoTitle.setTextColor(blackColor);
        txvPaleoContent.setTextColor(blackColor);
        txvKetoTitle.setTextColor(blackColor);
        txvKetoContent.setTextColor(blackColor);
    }

    private String getPointParameters() {
        pointParameters = "";
        if (chPoint1.isChecked()){
            pointParameters += Constants.NO_PROCESSED_FOOD + "_";
        }
        if (chPoint2.isChecked()){
            pointParameters += Constants.NOT_GOING + "_";
        }
        if (chPoint3.isChecked()){
            pointParameters += Constants.CONSUME + "_";
        }
        if (chPoint4.isChecked()){
            pointParameters += Constants.WOD + "_";
        }
        if (chPoint5.isChecked()){
            pointParameters += Constants.SLEEP + "_";
        }
        if (chPoint6.isChecked()){
            pointParameters += Constants.BENCHMARK + "_";
        }
        if (chPoint7.isChecked()){
            pointParameters += Constants.VARIOUS + "_";
        }
        if (chPoint8.isChecked()){
            pointParameters += Constants.FAT + "_";
        }
        if (chPoint9.isChecked()){
            pointParameters += Constants.FIST + "_";
        }
        if (chPoint10.isChecked()){
            pointParameters += Constants.VEGETABLES + "_";
        }
        if (chPoint11.isChecked()){
            pointParameters += Constants.FRUIT + "_";
        }
        if (chPoint12.isChecked()){
            pointParameters += Constants.PASTA;
        }

        if (pointParameters.length() > 0){
            if (pointParameters.substring(pointParameters.length() - 1).equals("_")){
                pointParameters = pointParameters.substring(0, pointParameters.length() - 1);
            }
        }

        return pointParameters;
    }

    private void createGroup() {



        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        groupId =  response.getInt(ReqConst.GROUP_ID);
                        if (kindOfGroup.equals(Constants.PUBLIC)){
                            btnAddMember.setVisibility(View.GONE);
                        }

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        CreateGroupRequest req = new CreateGroupRequest(String.valueOf(Commons.user.getId()), groupName, kindOfGroup, groupType, pointParameters, price, Commons.user.getUserName(), startDate, endDate, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void inviteMember() {
        if (invitedUser != null){
            if (invitedUser.getId() > 0){

                sendPushForInvite();
            }
        }else {
            inviteUnregisteredUser();
        }
    }

    private void inviteUnregisteredUser() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){
                        activity.showBottomToast("Member is added successfully");

                        MemberModel model = new MemberModel();
                        model.setName(memberName);
                        model.setEmail(memberEmail);
                        model.setPhoneNumber(memberPhoneNumber);

                        mMemberListAdapter.addMember(model);

                    }else if (resultCode ==  101){

                        MemberModel model = new MemberModel();
                        model.setName(memberName);
                        model.setEmail(memberEmail);
                        model.setPhoneNumber(memberPhoneNumber);

                        mMemberListAdapter.addMember(model);

                        activity.showBottomToast("Invitation sent");
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        SendPushForInviteRequest req = new SendPushForInviteRequest(String.valueOf(0), String.valueOf(groupId), memberName, memberEmail, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void sendPushForInvite() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){
                        activity.showBottomToast("Member is added successfully");

                        MemberModel model = new MemberModel();
                        model.setName(memberName);
                        model.setEmail(memberEmail);
                        model.setPhoneNumber(memberPhoneNumber);

                        mMemberListAdapter.addMember(model);

                    }else if (resultCode ==  101){
                        activity.showBottomToast("Invitation sent");
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        SendPushForInviteRequest req = new SendPushForInviteRequest(String.valueOf(invitedUser.getId()), String.valueOf(groupId), Commons.user.getUserName(), invitedUser.getEmail(), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void selectDate() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, android.app.AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);

        calendar.set(year, month, day);
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int y, int m, int d) {
        String tempMonth = "", tempDay = "", tempYear = String.valueOf(y);

        if (m < 10){
            tempMonth = "0" + (m+1);
        }else {
            tempMonth = String.valueOf(++m);
        }

        if (d < 10){
            tempDay = "0" + d;
        }else {
            tempDay = String.valueOf(d);
        }

        String date =tempYear + "/" + tempMonth + "/" + tempDay;

        if (startOrEndDate.equals(START_DATE)){
            txvStartDate.setText(date);
            startDate =  date;
        }else if (startOrEndDate.equals(END_DATE)){
            endDate = date;
            txvEndDate.setText(date);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvNext:
                index++;
                if (index == 1){
                    groupName = edtGroupName.getText().toString().trim();
                    if (groupName.length() == 0){
                        activity.showAlertDialog("Please input challenge name");
                        index --;
                        return;
                    }else if (kindOfGroup.length() == 0){
                        activity.showAlertDialog("Please select private or public group");
                        index --;
                        return;
                    }else if (startDate.length() == 0){
                        activity.showAlertDialog("Please select start date");
                        index --;
                        return;
                    }else if (endDate.length() == 0){
                        activity.showAlertDialog("Please select end date");
                        index --;
                        return;
                    }
                }else if (index == 2){
                    if (groupType.length() == 0){
                        activity.showAlertDialog("Please select General or Macro");
                        index--;
                        return;
                    }
                }else if (index == 3){
                    pointParameters = getPointParameters();
                    if (pointParameters.length() == 0){
                        activity.showAlertDialog("Please choice one point");
                        index --;
                        return;
                    }
                }else if (index == 4){
                    price = edtPrice.getText().toString().trim();
                    if (price.length() == 0){
                        activity.showAlertDialog("Please input price");
                        index --;
                        return;
                    }
                }
                addChildView();
                break;
            case R.id.imvPrev:
                index--;
                addChildView();
                break;
            case R.id.lytPrivate:
                choicePrivate();
                break;
            case R.id.lytPublic:
                choicePublic();
                break;
            case R.id.txvGeneralContent:
                choiceGeneral();
                break;
            case R.id.txvMacroContent:
                choiceMacroContent();
                break;
            case R.id.txvPaleoContent:
                choicePaleoMacroContent();
                break;
            case R.id.txvKetoContent:
                choiceKetoContent();
                break;
            case R.id.btnSubmit:
                activity.showBottomToast("<"+groupName + ">"+" created and invites sent out!");
                activity.showAlertDialog("Congrats on making your group! Make sure everyone is ready to participate by the <" + startDate + ">");
                activity.gotoJoinGroupFragment();
                index = 0;
                break;
            case R.id.btnAddMember:
                memberName = edtName.getText().toString().trim(); if (memberName.length() == 0) { activity.showBottomToast("Input member name!"); return;}
                memberEmail = edtEmail.getText().toString().trim(); if (memberEmail.length() == 0){activity.showBottomToast("Input member email!"); return;}
                memberPhoneNumber = edtPhoneNumber.getText().toString().trim();
                inviteMember();
                invitedUserCnt++;
                break;
            case R.id.txvStartDate:
                startOrEndDate = START_DATE;
                selectDate();
                break;
            case R.id.txvEndDate:
                startOrEndDate = END_DATE;
                selectDate();
                break;
        }
    }
}
