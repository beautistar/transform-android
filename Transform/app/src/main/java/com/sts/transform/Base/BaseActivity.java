package com.sts.transform.Base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.LoginRequest;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Models.MacroPointModel;
import com.sts.transform.Models.PointModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.Models.UserPropertyModel;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HGS on 12/11/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements Handler.Callback, View.OnClickListener {

    public Context _context = null;

    public Handler _handler = null;

    private ProgressDialog _progressDlg;

    private Vibrator _vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _context = this;

        _vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);

    }

    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)
                _vibrator.cancel();
        } catch (Exception e) {
        }
        _vibrator = null;

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void showProgress(boolean cancelable) {

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg
                .setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();
    }

    public void showProgress() {
        showProgress(false);
    }

    public void closeProgress() {

        if(_progressDlg == null) {
            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }

    public void showAlertDialog(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        alertDialog.show();

    }

    /**
     *  show toast
     * @param toast_string
     */
    public void showCenterToast(String toast_string) {
        Toast toast = Toast.makeText(_context ,toast_string , Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void showBottomToast(String toast_string) {

        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    public void vibrate() {

        if (_vibrator != null)
            _vibrator.vibrate(500);
    }

    public float roundFloat(float f) {
        /*int c = (int) ((f) + 0.5f);
        float n = f + 0.5f;
        return (n - c) % 2 == 0 ? (int) f : c;*/
        String s = String.format("%.2f", f);
        return Float.parseFloat(s);
    }

    public int roundInt(float f) {
        int c = (int) ((f) + 0.5f);
        float n = f + 0.5f;
        return (n - c) % 2 == 0 ? (int) f : c;
    }

    public void hideSoftKeyboard() {
        InputMethodManager keyboard =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this != null && getCurrentFocus() != null) {
            keyboard.hideSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean handleMessage(Message msg) {

        switch (msg.what) {

            default:
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {

    }

    public void refreshUserInfo() {
        Log.d("aaaaa", Commons.user.getPassword());

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setUserName(jsonUser.getString(ReqConst.USER_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPassword(Commons.user.getPassword());
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        UserPropertyModel userPropertyModel = new UserPropertyModel();
                        JSONObject jsonPropertyModel = response.getJSONObject(ReqConst.PROPERTY_MODEL);
                        userPropertyModel.setGender(jsonPropertyModel.getString(ReqConst.GENDER));
                        userPropertyModel.setBirthday(jsonPropertyModel.getString(ReqConst.BIRTHDAY));
                        userPropertyModel.setActivityFactor(jsonPropertyModel.getString(ReqConst.ACTIVITY_FACTOR));
                        userPropertyModel.setAge(jsonPropertyModel.getString(ReqConst.AGE));
                        userPropertyModel.setHeight(jsonPropertyModel.getString(ReqConst.HEIGHT));
                        userPropertyModel.setBodyWeight(jsonPropertyModel.getString(ReqConst.BODY_WEIGHT));
                        userPropertyModel.setAverageWaist(jsonPropertyModel.getString(ReqConst.AVERAGE_WAIST));
                        userPropertyModel.setAverageWrist(jsonPropertyModel.getString(ReqConst.AVERAGE_WRIST));
                        userPropertyModel.setAverageHip(jsonPropertyModel.getString(ReqConst.AVERAGE_HIP));
                        userPropertyModel.setAverageAbdomen(jsonPropertyModel.getString(ReqConst.AVERAGE_ABDOMEN));
                        userPropertyModel.setStartingBodyWeight(jsonPropertyModel.getString(ReqConst.STARTING_BODY_WEIGHT));

                        userModel.setUserPropertyModel(userPropertyModel);

                        MacroPointModel mMacroPointModel = new MacroPointModel();
                        JSONObject jsonMacroPointModel = response.getJSONObject(ReqConst.MACRO_MODEL);
                        mMacroPointModel.setBodyFat(jsonMacroPointModel.getString(ReqConst.BODY_FAT));
                        mMacroPointModel.setBodyMass(jsonMacroPointModel.getString(ReqConst.BODY_Mass));
                        mMacroPointModel.setProtein(jsonMacroPointModel.getString(ReqConst.DAILY_Protein));
                        mMacroPointModel.setCarb(jsonMacroPointModel.getString(ReqConst.DAILY_Carb));
                        mMacroPointModel.setFat(jsonMacroPointModel.getString(ReqConst.DAILY_Fat));
                        mMacroPointModel.setTotalBlocks(jsonMacroPointModel.getString(ReqConst.TOTAL_Blocks));
                        mMacroPointModel.setEatBlocksTillNow(jsonMacroPointModel.getString(ReqConst.EAT_BLOCKS_TILL_NOW));
                        mMacroPointModel.setStartingBodyFatPercent(jsonMacroPointModel.getString(ReqConst.STARTING_BODY_FAT_PERCENT));

                        userModel.setMacroPointModel(mMacroPointModel);

                        ArrayList<PointModel> mPointModelList = new ArrayList<>();
                        JSONArray jsonPointModelList = response.getJSONArray(ReqConst.POINT_MODEL_LIST);
                        for (int i =  0; i < jsonPointModelList.length(); i ++){
                            PointModel mPointModel =  new PointModel();
                            JSONObject jsonPointModel = (JSONObject) jsonPointModelList.get(i);
                            mPointModel.setGroupId(jsonPointModel.getInt(ReqConst.GROUP_ID));
                            mPointModel.setTodayPoints(jsonPointModel.getInt(ReqConst.TODAY_POINTS));
                            mPointModel.setTotalPoints(jsonPointModel.getInt(ReqConst.TOTAL_POINTS));

                            mPointModelList.add(mPointModel);
                        }

                        userModel.setPointModels(mPointModelList);

                        Commons.user = userModel;

                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginRequest req = new LoginRequest(Commons.user.getEmail(), Commons.user.getPassword(), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }
}
