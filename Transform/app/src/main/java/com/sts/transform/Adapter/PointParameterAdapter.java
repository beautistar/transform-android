package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.AddPointRequest;
import com.sts.transform.API.GetGroupInfoRequest;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 11/26/2017.
 */

public class PointParameterAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<String> allData = new ArrayList<>();

    public PointParameterAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(String[] data){
        allData.clear();
        for (int i = 0; i < data.length; i++){
            allData.add(data[i]);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){

            holder = new CustomHolder();

            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_point_parameter, parent, false);

            holder.setId(convertView);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        String pointContent = allData.get(position);
        holder.setData(pointContent);

        return convertView;
    }

    private class CustomHolder{
        CheckBox chPoint;

        private void setId(View view){
            chPoint = (CheckBox)view.findViewById(R.id.chPoint);
            chPoint.setClickable(false);
            chPoint.setChecked(true);
        }

        private void setData(String point){
            String pointContent = "";

            switch (point){
                case Constants.NO_PROCESSED_FOOD:
                    pointContent = Constants.pointContent1;
                    break;
                case Constants.NOT_GOING:
                    pointContent = Constants.pointContent2;
                    break;
                case Constants.CONSUME:
                    pointContent = Constants.pointContent3;
                    break;
                case Constants.WOD:
                    pointContent = Constants.pointContent4;
                    break;
                case Constants.SLEEP:
                    pointContent = Constants.pointContent5;
                    break;
                case Constants.BENCHMARK:
                    pointContent = Constants.pointContent6;
                    break;
                case Constants.VARIOUS:
                    pointContent = Constants.pointContent7;
                    break;
                case Constants.FAT:
                    pointContent = Constants.pointContent8;
                    break;
                case Constants.FIST:
                    pointContent = Constants.pointContent9;
                    break;
                case Constants.VEGETABLES:
                    pointContent = Constants.pointContent10;
                    break;
                case Constants.FRUIT:
                    pointContent = Constants.pointContent11;
                    break;
                case Constants.PASTA:
                    pointContent = Constants.pointContent12;
                    break;
            }

            chPoint.setText(pointContent);
        }
    }
}
