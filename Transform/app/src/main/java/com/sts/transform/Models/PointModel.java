package com.sts.transform.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/16/2017.
 */

public class PointModel implements Serializable {

    int groupId = 0, todayPoints = 0, totalPoints = 0;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getTodayPoints() {
        return todayPoints;
    }

    public void setTodayPoints(int todayPoints) {
        this.todayPoints = todayPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }
}
