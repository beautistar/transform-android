package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.transform.API.GetRankingListInGroupRequest;
import com.sts.transform.API.LoginRequest;
import com.sts.transform.API.UpdateMacroPointRequest;
import com.sts.transform.API.UploadPersonalInfoRequest;
import com.sts.transform.Adapter.PrevChallengeListAdapter;
import com.sts.transform.Adapter.RankingListAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.MacroPointModel;
import com.sts.transform.Models.RankingModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.Models.UserPropertyModel;
import com.sts.transform.R;
import com.sts.transform.TransformApplication;
import com.sts.transform.Utils.BitmapUtils;
import com.sts.transform.Utils.MultiPartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class ProfileFragment extends BaseFragment {

    MainActivity activity;
    RankingListAdapter mRankingListAdapter;
    PrevChallengeListAdapter mPrevChallengeAdapter;

    Button btnEdit;
    RoundedImageView imvPhoto;
    TextView txvGender, txvAge, txvBodyWeight, txvActivityFactor, txvWristHip, txvWaistAbdomen, txvBodyFat, txvBodyMass, txvDailyProtein, txvTotalBlocks, txvCarb, txvDailyFat;
    TextView txvHeight, txvWaistAbdomenTitle, txvWristHipTitle, txvChallenge, txvName;
    ExpandableHeightListView lstRankingList;
    ListView lstPrevChallengelist;

    private Uri imageCaptureUri;
    String photoPath = "", startingBodyFatPercent = "";
    float bodyFat = 0.0f, bodyMass = 0.0f, dailyProtein = 0.0f, dailyFat = 0.0f, femaleA = 0.0f, femaleB = 0.0f, femaleC = 0.0f;
    float totalBlocks = 0, dailyCarb = 0;

    ArrayList<RankingModel> mRankingModelList = new ArrayList<>();
    ArrayList<UserModel> mUserModelList;
    GroupModel selectedGroup = new GroupModel();

    public ProfileFragment(MainActivity activity) {
        this.activity = activity;
        startingBodyFatPercent = Commons.user.getMacroPointModel().getStartingBodyFatPercent();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_profile, container, false);

        txvName = (TextView)fragment.findViewById(R.id.txvName);
        txvName.setText(Commons.user.getUserName());

        mRankingListAdapter =  new RankingListAdapter(activity);
        lstRankingList = (ExpandableHeightListView) fragment.findViewById(R.id.lstRankingList);
        lstRankingList.setAdapter(mRankingListAdapter);
        mRankingListAdapter.refresh(Commons.rankingModelList);
        lstRankingList.setExpanded(true);

        mPrevChallengeAdapter = new PrevChallengeListAdapter(activity);
        lstPrevChallengelist = (ListView) fragment.findViewById(R.id.lstPrevList);
        lstPrevChallengelist.setAdapter(mPrevChallengeAdapter);
        mPrevChallengeAdapter.refresh(Commons.prevChallengeList);
        lstPrevChallengelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedGroup = Commons.prevChallengeList.get(position);
                getGroupRanking();
            }
        });

        txvWaistAbdomenTitle = (TextView)fragment.findViewById(R.id.txvWaistAbdomenTitle);
        txvWristHipTitle = (TextView)fragment.findViewById(R.id.txvWristHipTitle);

        txvHeight = (TextView)fragment.findViewById(R.id.txvHeight);

        txvDailyFat = (TextView)fragment.findViewById(R.id.txvDailyFat);

        txvTotalBlocks = (TextView)fragment.findViewById(R.id.txvTotalBlocks);

        txvCarb = (TextView)fragment.findViewById(R.id.txvCarb);

        txvDailyProtein = (TextView)fragment.findViewById(R.id.txvDailyProtein);

        txvActivityFactor = (TextView)fragment.findViewById(R.id.txvActivityFactor);
        txvActivityFactor.setText(Commons.user.getUserPropertyModel().getActivityFactor());

        txvGender = (TextView)fragment.findViewById(R.id.txvGender);
        txvGender.setText(Commons.user.getUserPropertyModel().getGender());

        txvBodyWeight = (TextView)fragment.findViewById(R.id.txvBodyWeight);
        txvBodyWeight.setText(String.valueOf(Commons.user.getUserPropertyModel().getBodyWeight()));

        txvAge = (TextView)fragment.findViewById(R.id.txvAge);
        txvAge.setText(String.valueOf(Commons.user.getUserPropertyModel().getAge()));

        btnEdit = (Button)fragment.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(this);

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);
        if (Commons.user.getPhotoUrl().length() > 0){
            Picasso.with(activity).load(Commons.user.getPhotoUrl()).into(imvPhoto);
        }

        txvWristHip = (TextView)fragment.findViewById(R.id.txvWristHip);
        txvWaistAbdomen = (TextView)fragment.findViewById(R.id.txvWaistAbdomen);

        txvBodyFat = (TextView)fragment.findViewById(R.id.txvBodyFat);
        txvBodyMass = (TextView)fragment.findViewById(R.id.txvBodyMass);

        return fragment;
    }

    private String calculateBodyFatForMale() {

        float averageWaist = 0.0f, averageWrist = 0.0f, averageHip = 0.0f, averageAbdomen = 0.0f, height = 0.0f, bodyFat = 0.0f, bodyFatPercent = 0.0f;
        int row = 0, column = 0;

        if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE)){

            if (Commons.user.getUserPropertyModel().getAverageWaist().length() > 0){
                averageWaist =Float.parseFloat(Commons.user.getUserPropertyModel().getAverageWaist());
            }

            if (Commons.user.getUserPropertyModel().getAverageWrist().length() > 0){
                averageWrist = Float.parseFloat(Commons.user.getUserPropertyModel().getAverageWrist());
            }

            bodyFat = averageWaist - averageWrist;

        }

        for (int i = 1; i < 38; i ++) {
            if (Commons.bodyFatPercentTableForMale[i][0].length() > 0 && Commons.user.getUserPropertyModel().getBodyWeight().length() > 0){
                if (Float.parseFloat(Commons.user.getUserPropertyModel().getBodyWeight()) == Float.parseFloat(Commons.bodyFatPercentTableForMale[i][0])){
                    row = i;
                }
            }
        }

        for (int i = 1; i < 58 ; i++){
            if (Commons.bodyFatPercentTableForMale[0][i].length() > 0){
                if (bodyFat ==  Float.parseFloat(Commons.bodyFatPercentTableForMale[0][i])){
                    column = i;
                }
            }
        }

        if (Commons.bodyFatPercentTableForMale[row][column].length() > 0 && Commons.user.getUserPropertyModel().getBodyWeight().length() > 0){
            bodyFatPercent = Float.parseFloat(Commons.bodyFatPercentTableForMale[row][column]);
            if (bodyFatPercent >= 50) {bodyFatPercent = 50;}
            bodyFat = bodyFatPercent * Float.parseFloat(Commons.user.getUserPropertyModel().getBodyWeight())/100;

        }else if (Commons.bodyFatPercentTableForMale[row][column].length() == 0){
            bodyFatPercent = 55;
            bodyFat = bodyFatPercent * Float.parseFloat(Commons.user.getUserPropertyModel().getBodyWeight())/100;
        }

        if (Commons.user.getMacroPointModel().getStartingBodyFatPercent().equals("0.00") || Commons.user.getMacroPointModel().getStartingBodyFatPercent().equals("") ){
            if (bodyFatPercent > 0.01f){
                startingBodyFatPercent = String.valueOf(bodyFatPercent);
                Commons.user.getMacroPointModel().setStartingBodyFatPercent(startingBodyFatPercent);
            }
        }


        return String.valueOf(bodyFat);
    }

    private String calculateBodyMass() {

        float bodyWeight = 0.0f;

        if (Commons.user.getUserPropertyModel().getBodyWeight().length() > 0){
            bodyWeight = Float.parseFloat(Commons.user.getUserPropertyModel().getBodyWeight());
        }

        if (txvBodyFat.length() > 0){
            bodyFat = Float.parseFloat(txvBodyFat.getText().toString().trim());
        }

        bodyMass = bodyWeight - bodyFat;
        bodyMass = activity.roundFloat(bodyMass);

        return String.valueOf(bodyMass);
    }

    private String calculateDailyProtein() {

        float activityFactor = 0.0f;
        switch (Commons.user.getUserPropertyModel().getActivityFactor()){
            case Constants.SEDENTARY:
                activityFactor = Constants.SEDENTARY_VALUE;
                break;
            case Constants.LIGHT:
                activityFactor = Constants.LIGHT_VALUE;
                break;
            case Constants.MODERATE:
                activityFactor = Constants.MODERATE_VALUE;
                break;
            case Constants.ACTIVE:
                activityFactor = Constants.ACTIVE_VALUE;
                break;
            case Constants.VERY_ACTIVE:
                activityFactor = Constants.VERY_ACTIVE_VALUE;
                break;
            case Constants.HEAVY_WEIGHT_TRAINING:
                activityFactor = Constants.HEAVY_WEIGHT_TRAINING_VALUE;
                break;
        }

        dailyProtein = bodyMass * activityFactor;
        dailyProtein = activity.roundFloat(dailyProtein);

        return String.valueOf(dailyProtein);
    }

    private String calculateTotalBlocks() {
        totalBlocks = activity.roundFloat(dailyProtein/7);
        return String.valueOf(activity.roundInt(totalBlocks));
    }

    private String calculateCarbForMale() {
        dailyCarb = totalBlocks * 9;
        dailyCarb = activity.roundFloat(dailyCarb);
        return String .valueOf(dailyCarb);
    }

    private String calculateDailyFat() {
        dailyFat = totalBlocks * 1.5f;
        dailyFat =  activity.roundFloat(dailyFat);
        return String.valueOf(dailyFat);
    }

    private float getAFromTable(String tempHip) {

        if (tempHip.length() == 0) return 0.0f;
        float hip = Float.parseFloat(tempHip);

        for (int i = 1; i< 62; i ++){
            if (hip == Float.parseFloat(Commons.abcTableForFemale[i][0])){
                femaleA = Float.parseFloat(Commons.abcTableForFemale[i][1]);
            }
        }
        return femaleA;
    }

    private float getBFromTable(String tempAbdomen) {

        if (tempAbdomen.length() == 0) return 0.0f;

        float  abdomen = Float.parseFloat(tempAbdomen);

        for (int i = 1; i < 62; i++){
            if (abdomen == Float.parseFloat(Commons.abcTableForFemale[i][2])){
                femaleB = Float.parseFloat(Commons.abcTableForFemale[i][3]);
            }
        }
        return femaleB;
    }

    private float getCFromTable(String tempHeight) {

        if (tempHeight.length() == 0) return 0.0f;

        float height = Float.parseFloat(tempHeight);

        for (int i = 1 ; i < 44; i ++){
            if (height == Float.parseFloat(Commons.abcTableForFemale[i][4])){
                femaleC = Float.parseFloat(Commons.abcTableForFemale[i][5]);
            }
        }

        return femaleC;
    }

    private String calculateBodyFatForFemale() {

        float bodyFatPercent = femaleA + femaleB - femaleC;
        bodyFatPercent = activity.roundFloat(bodyFatPercent);
        if (bodyFatPercent > 50) bodyFatPercent = 50;

        if (Commons.user.getUserPropertyModel().getBodyWeight().length() > 0){
            bodyFat = bodyFatPercent * Float.parseFloat(Commons.user.getUserPropertyModel().getBodyWeight())/100;
            bodyFat = activity.roundFloat(bodyFat);
        }

        if (Commons.user.getMacroPointModel().getStartingBodyFatPercent().equals("0.00") || Commons.user.getMacroPointModel().getStartingBodyFatPercent().equals("")){

            if (bodyFatPercent > 0.01f){
                startingBodyFatPercent = String.valueOf(bodyFatPercent);
                Commons.user.getMacroPointModel().setStartingBodyFatPercent(startingBodyFatPercent);
            }
        }

        return String.valueOf(bodyFat);
    }

    private void takePhoto() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takePhotoFromCamera();

                } else {
                    takePhotoFromGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void getGroupRanking() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        mRankingModelList.clear();
                        JSONArray jsonRankingModelList = response.getJSONArray(ReqConst.RANKING_LIST);

                        mUserModelList =  new ArrayList<>();

                        for (int i = 0; i < jsonRankingModelList.length(); i++){

                            UserModel mUserModel = new UserModel();

                            RankingModel mRankingModel = new RankingModel();
                            JSONObject jsonRankingModel = (JSONObject) jsonRankingModelList.get(i);

                            mRankingModel.setUserId(jsonRankingModel.getInt(ReqConst.USER_ID));
                            mRankingModel.setTotalPoints(jsonRankingModel.getInt(ReqConst.TOTAL_POINTS));
                            mRankingModel.setUserName(jsonRankingModel.getString(ReqConst.USER_NAME));
                            mRankingModel.setPhotoUrl(jsonRankingModel.getString(ReqConst.PHOTO_URL));

                            mUserModel.setId(jsonRankingModel.getInt(ReqConst.USER_ID));
                            mUserModel.setUserName(jsonRankingModel.getString(ReqConst.USER_NAME));
                            mUserModel.setBenchmark(jsonRankingModel.getInt(ReqConst.BENCHMARK));
                            mUserModel.setPhotoUrl(jsonRankingModel.getString(ReqConst.PHOTO_URL));
                            mUserModel.setTotalPointOfSelectedGroup(jsonRankingModel.getInt(ReqConst.TOTAL_POINTS));



                            mUserModelList.add(mUserModel);

                            mRankingModelList.add(mRankingModel);
                        }

                        Collections.sort(mRankingModelList, new Comparator<RankingModel>() {
                            @Override
                            public int compare(RankingModel o1, RankingModel o2) {

                                return Integer.valueOf(o2.getTotalPoints()).compareTo(Integer.valueOf(o1.getTotalPoints()));
                            }
                        });

                        Commons.rankingModelList = mRankingModelList;

                        showRankingList();


                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetRankingListInGroupRequest req = new GetRankingListInGroupRequest(String.valueOf(selectedGroup.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }



    private void showRankingList() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ranking_list);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        RankingListAdapter mRankingListAdapter = new RankingListAdapter(activity);
        ListView lstRankingList = (ListView)dialog.findViewById(R.id.lstRankingList);
        lstRankingList.setAdapter(mRankingListAdapter);
        mRankingListAdapter.refresh(mRankingModelList);

        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(activity);

                        InputStream in = activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();

                        uploadPhoto();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(activity, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadPhoto() {
        try {

            activity.showProgress();
            File file = new File(photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.USER_ID, String.valueOf(Commons.user.getId()));

            String url = ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_PHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    activity.closeProgress();

                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){

                            Commons.user.setPhotoUrl(response.getString(ReqConst.PHOTO_URL));

                        }else {
                            activity.showAlertDialog(getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            TransformApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            activity.closeProgress();
            activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Commons.user != null){

            txvActivityFactor.setText(Commons.user.getUserPropertyModel().getActivityFactor());
            txvGender.setText(Commons.user.getUserPropertyModel().getGender());
            txvBodyWeight.setText(String.valueOf(Commons.user.getUserPropertyModel().getBodyWeight()));
            txvAge.setText(String.valueOf(Commons.user.getUserPropertyModel().getAge()));
            txvHeight.setText(Commons.user.getUserPropertyModel().getHeight());

            if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE)){

                txvWristHipTitle.setText("Wrist : ");
                txvWaistAbdomenTitle.setText("Waist : ");

                txvWristHip.setText(String.valueOf(Commons.user.getUserPropertyModel().getAverageWrist()));
                txvWaistAbdomen.setText(String.valueOf(Commons.user.getUserPropertyModel().getAverageWaist()));

                if (txvWristHip.getText().toString().trim().equals("") || txvWristHip.getText().toString().trim().equals("0.00") || txvWaistAbdomen.getText().toString().trim().equals("") || txvWaistAbdomen.getText().toString().trim().equals("0.00")) return;

                txvBodyFat.setText(calculateBodyFatForMale());
                txvBodyMass.setText(calculateBodyMass());
                txvDailyProtein.setText(calculateDailyProtein());
                txvTotalBlocks.setText(calculateTotalBlocks());
                txvCarb.setText(calculateCarbForMale());
                txvDailyFat.setText(calculateDailyFat());

                updateUserProperty();

            }else if (Commons.user.getUserPropertyModel().getGender().equals(Constants.FEMALE)){

                txvWristHipTitle.setText("Hip : ");
                txvWaistAbdomenTitle.setText("Abdomen : ");

                txvWristHip.setText(Commons.user.getUserPropertyModel().getAverageHip());
                txvWaistAbdomen.setText(Commons.user.getUserPropertyModel().getAverageAbdomen());

                if (txvWristHip.getText().toString().trim().equals("") || txvWristHip.getText().toString().trim().equals("0.00") || txvWaistAbdomen.getText().toString().trim().equals("") || txvWaistAbdomen.getText().toString().trim().equals("0.00")) return;

                femaleA = getAFromTable(txvWristHip.getText().toString().trim());
                femaleB =  getBFromTable(txvWaistAbdomen.getText().toString().trim());
                femaleC = getCFromTable(txvHeight.getText().toString().trim());

                txvBodyFat.setText(calculateBodyFatForFemale());
                txvBodyMass.setText(calculateBodyMass());
                txvDailyProtein.setText(calculateDailyProtein());
                txvTotalBlocks.setText(calculateTotalBlocks());
                txvCarb.setText(calculateCarbForMale());
                txvDailyFat.setText(calculateDailyFat());
                updateUserProperty();
            }
        }
    }

    private void updateUserProperty() {
        final String bodyFat = txvBodyFat.getText().toString().trim();
        final String bodyMass = txvBodyMass.getText().toString().trim();
        final String dailyProtein = txvDailyProtein.getText().toString().trim();
        final String dailyCarb = txvCarb.getText().toString().trim();
        final String dailyFat = txvDailyFat.getText().toString().trim();
        final String totalBlocks = txvTotalBlocks.getText().toString().trim();

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        MacroPointModel mMacroPointModel = new MacroPointModel();
                        mMacroPointModel.setBodyFat(bodyFat);
                        mMacroPointModel.setBodyMass(bodyMass);
                        mMacroPointModel.setProtein(dailyProtein);
                        mMacroPointModel.setCarb(dailyCarb);
                        mMacroPointModel.setFat(dailyFat);
                        mMacroPointModel.setTotalBlocks(totalBlocks);
                        mMacroPointModel.setStartingBodyFatPercent(startingBodyFatPercent);

                        Commons.user.setMacroPointModel(mMacroPointModel);

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();

        UpdateMacroPointRequest req = new UpdateMacroPointRequest(String.valueOf(Commons.user.getId()), bodyFat, bodyMass, dailyProtein, dailyCarb, dailyFat, totalBlocks, startingBodyFatPercent, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEdit:
                activity.gotoEditFragment();
                break;
            case R.id.imvPhoto:
                takePhoto();
                break;
        }
    }
}
