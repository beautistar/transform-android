package com.sts.transform.Main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sts.transform.API.SaveTokenRequest;
import com.sts.transform.API.SignupRequest;
import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends BaseActivity {

    Button btnBack, btnSignup;
    EditText edtUserName, edtEmail, edtPassword, edtConfirmPwd, edtPhoneNumber;
    String email = "", password = "", confirmPwd = "", userName = "", phone = "";
    int userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        loadLayout();
    }

    private void loadLayout() {
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);

        edtUserName = (EditText)findViewById(R.id.edtUserName);
        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtConfirmPwd = (EditText)findViewById(R.id.edtConfirmPwd);
        edtPhoneNumber = (EditText)findViewById(R.id.edtPhoneNumber);
    }

    private void gotoLoginAcitivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean valid() {

        userName = edtUserName.getText().toString().trim();
        email =  edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        confirmPwd =  edtConfirmPwd.getText().toString().trim();

        if (userName.length() == 0){
            showAlertDialog("Please input user name");
            return false;
        }else if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        } else if (password.length() ==  0){
            showAlertDialog("Please input password");
            return false;
        }else if (confirmPwd.length() ==  0 || !password.equals(confirmPwd)){
            showAlertDialog("Please confirm password");
            return false;
        }

        return true;
    }

    private void signupWithEmail() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        userId  = response.getInt(ReqConst.USER_ID);
                        saveToken(userId);
                    }else if (resultCode == ReqConst.CODE_ALREADY_REGISTERED){
                        showAlertDialog("Already registered");
                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SignupRequest req = new SignupRequest(userName, email, password, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void gotoProsonalInfoActivity() {

        UserModel userModel = new UserModel();
        userModel.setId(userId);
        userModel.setUserName(userName);
        userModel.setEmail(email);
        userModel.setPassword(password);

        Commons.user = userModel;

        Intent intent = new Intent(this, PersonalInfoActivity.class);
        intent.putExtra(Constants.ID, userId);
        startActivity(intent);
        finish();
    }

    private void saveToken(int userId){

        String token = FirebaseInstanceId.getInstance().getToken();
        if (token.length() == 0){
            showAlertDialog("Network error!");
            return;
        }

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        gotoProsonalInfoActivity();

                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SaveTokenRequest req = new SaveTokenRequest(String.valueOf(userId), token, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBack:
                gotoLoginAcitivity();
                break;
            case R.id.btnSignup:
                if (valid()){
                    signupWithEmail();
                }
                break;
        }
    }
}
