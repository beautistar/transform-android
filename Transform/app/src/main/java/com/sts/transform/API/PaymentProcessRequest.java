package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class PaymentProcessRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_PAYMENT_PROCESS;
    private Map<String,String> params;

    public PaymentProcessRequest(String userId, String groupId, String userName, String email, String cardNumber,
                                 String expirationDate, String  cardCode, String amount, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
        params.put(ReqConst.GROUP_ID, groupId);
        params.put(ReqConst.USER_NAME, userName);
        params.put(ReqConst.EMAIL, email);
        params.put(ReqConst.CARD_NUMBER, cardNumber);
        params.put(ReqConst.EXPIRATION_DATA, expirationDate);
        params.put(ReqConst.CARD_CODE, cardCode);
        params.put(ReqConst.AMOUNT, amount);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}