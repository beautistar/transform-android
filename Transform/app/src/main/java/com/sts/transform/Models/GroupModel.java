package com.sts.transform.Models;

import java.io.Serializable;

/**
 * Created by STS on 11/20/2017.
 */

public class GroupModel implements Serializable {
    int id = 0, creatorId = 0;
    String kindOfGroup = "", groupName = "", groupType = "", pointParameter = "", priceOfGroup = "" , creatorName = "", startDate = "", endDate = "";
    String challengeFinished = "";

    public String getChallengeFinished() {
        return challengeFinished;
    }

    public void setChallengeFinished(String challengeFinished) {
        this.challengeFinished = challengeFinished;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getKindOfGroup() {
        return kindOfGroup;
    }

    public void setKindOfGroup(String kindOfGroup) {
        this.kindOfGroup = kindOfGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getPointParameter() {
        return pointParameter;
    }

    public void setPointParameter(String pointParameter) {
        this.pointParameter = pointParameter;
    }

    public String getPriceOfGroup() {
        return priceOfGroup;
    }

    public void setPriceOfGroup(String priceOfGroup) {
        this.priceOfGroup = priceOfGroup;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
