package com.sts.transform.Main;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.UploadPersonalInfoRequest;
import com.sts.transform.Adapter.TakeHeightWeightAdapter;
import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Models.UserPropertyModel;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class PersonalInfoActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener{

    Button btnBack, btnNext;
    TextView txvGender, txvBirthday, txvAge, txvActivityFactor, txvHeight;
    TextView txvWeight;

    TakeHeightWeightAdapter mTakeHeightWeightAdapter;

    String gender = "", activityFactor = "", birthday = "", age = "", bodyWeight = "", height = "", startingBodyWeight = "";
    Calendar calendar;
    int year = 0, month = 0, day = 0, userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        userId = getIntent().getIntExtra(Constants.ID, 0);

        loadLayout();
        getCurrentDate();
    }

    private void loadLayout() {
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);

        txvGender = (TextView)findViewById(R.id.txvGender);
        txvGender.setOnClickListener(this);

        txvBirthday = (TextView)findViewById(R.id.txvBirthday);
        txvBirthday.setOnClickListener(this);
        txvAge = (TextView)findViewById(R.id.txvAge);

        txvActivityFactor = (TextView)findViewById(R.id.txvActivityFactor);
        txvActivityFactor.setOnClickListener(this);

        txvHeight = (TextView) findViewById(R.id.txvHeight);
        txvHeight.setOnClickListener(this);

        txvWeight = (TextView) findViewById(R.id.txvWeight);
        txvWeight.setOnClickListener(this);

    }

    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void gotoSignupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, Commons.user.getEmail());
        Preference.getInstance().put(this, PrefConst.PASSWORD, Commons.user.getPassword());

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.KEY_NEW_REGISTER, true);
        startActivity(intent);
        finish();
    }

    private void selectGender() {
        final String[] items = {"Male", "Female"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }
                txvGender.setText(gender);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takeBirthday(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(_context, android.app.AlertDialog.THEME_HOLO_LIGHT, this, 1980, 0, 1);

        calendar.set(year, month + 1, day);
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

        calendar.set(1980, 0, 1);
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        datePickerDialog.show();
    }

    private void showActivityFactorDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_activity_factor);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final RadioButton rbSedentary =  (RadioButton)dialog.findViewById(R.id.rbSedentary);
        final RadioButton rbLight= (RadioButton)dialog.findViewById(R.id.rbLight);
        final RadioButton rbModerate= (RadioButton)dialog.findViewById(R.id.rbModerate);
        final RadioButton rbActivity = (RadioButton)dialog.findViewById(R.id.rbActivity);
        final RadioButton rbVeryActivity = (RadioButton)dialog.findViewById(R.id.rbVeryActivity);
        final RadioButton rbHeavyActivity = (RadioButton)dialog.findViewById(R.id.rbHeavyActivity);

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbSedentary.isChecked()){
                    activityFactor = Constants.SEDENTARY;
                }else if (rbLight.isChecked()){
                    activityFactor = Constants.LIGHT;
                }else if (rbModerate.isChecked()){
                    activityFactor = Constants.MODERATE;
                }else if (rbActivity.isChecked()){
                    activityFactor = Constants.ACTIVE;
                }else if (rbVeryActivity.isChecked()){
                    activityFactor =  Constants.VERY_ACTIVE;
                }else if (rbHeavyActivity.isChecked()){
                    activityFactor = Constants.HEAVY_WEIGHT_TRAINING;
                }


                txvActivityFactor.setText(activityFactor);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    @Override
    public void onDateSet (DatePicker view, int y, int m, int d) {
        String tempMonth = "", tempDay = "", tempYear = String.valueOf(y);

        if (m < 10){
            tempMonth = "0" + (m+1);
        }else {
            tempMonth = String.valueOf(++m);
        }

        if (d < 10){
            tempDay = "0" + d;
        }else {
            tempDay = String.valueOf(d);
        }

        String date = tempMonth + "/" + tempDay + "/" + tempYear;
        txvBirthday.setText(date);
        txvAge.setText(String.valueOf(year - y));
    }

    private boolean valid() {
        gender = txvGender.getText().toString().trim();
        birthday = txvBirthday.getText().toString().trim();
        age = txvAge.getText().toString().trim();
        bodyWeight = txvWeight.getText().toString().trim();
        height = txvHeight.getText().toString().trim();
        activityFactor = txvActivityFactor.getText().toString().trim();
        startingBodyWeight = txvWeight.getText().toString().trim();

        if (gender.length() == 0){
            showAlertDialog("Please select gender");
            return false;
        }else if (birthday.length() == 0){
            showAlertDialog("Please select birthday");
            return false;
        }else if (age.length() == 0){
            showAlertDialog("Please input age");
            return false;
        }else if (bodyWeight.length() == 0){
            showAlertDialog("Please input body weight");
            return false;
        }else if (height.length() == 0){
            showAlertDialog("Please input height");
            return false;
        }else if (activityFactor.length() == 0){
            showAlertDialog("Please select activity factor");
            return false;
        }
        return true;
    }

    private void uploadPersonalInfo() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        UserPropertyModel userPropertyModel =  new UserPropertyModel();
                        userPropertyModel.setGender(gender);
                        userPropertyModel.setBirthday(birthday);
                        userPropertyModel.setAge(age);
                        userPropertyModel.setBodyWeight(bodyWeight);
                        userPropertyModel.setHeight(height);
                        userPropertyModel.setActivityFactor(activityFactor);
                        userPropertyModel.setStartingBodyWeight(startingBodyWeight);

                        Commons.user.setUserPropertyModel(userPropertyModel);

                        gotoMainActivity();
                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        UploadPersonalInfoRequest req = new UploadPersonalInfoRequest(String.valueOf(userId), gender, birthday, String.valueOf(age), String.valueOf(bodyWeight), String.valueOf(height), activityFactor, "", "", "", "", startingBodyWeight, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void takeHeight() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select height in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(this);
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.heightList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvHeight.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeWeight() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Weight in lbs");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(this);
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.weightList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvWeight.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBack:
                gotoSignupActivity();
                break;
            case R.id.btnNext:
                if (valid()){
                    uploadPersonalInfo();
                }
                break;
            case R.id.txvGender:
                selectGender();
                break;
            case R.id.txvBirthday:
                takeBirthday();
                break;
            case R.id.txvActivityFactor:
                showActivityFactorDialog();
                break;
            case R.id.txvHeight:
                takeHeight();
                break;
            case R.id.txvWeight:
                takeWeight();
                break;
        }
    }
}
