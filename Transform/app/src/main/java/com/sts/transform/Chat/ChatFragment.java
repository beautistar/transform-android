package com.sts.transform.Chat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.baoyz.widget.PullRefreshLayout;
import com.sts.transform.API.GetMessagesRequest;
import com.sts.transform.API.SendChatMessageRequest;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.LoginActivity;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Main.SplashActivity;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class ChatFragment extends BaseFragment {

    public static ChatFragment instance;

    MainActivity activity;
    ChatAdapter mChatAdapter;

    ListView lstChatList;
    ImageView imvBack, imvSend;
    PullRefreshLayout rlContainer;
    EditText edtMessage;
    TextView txvGroupName;

    int readCount = 0;
    String message = "";

    public ChatFragment(MainActivity activity) {
        this.activity = activity;
        Commons.isChatFragment = true;
        instance = this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_chat, container, false);

        mChatAdapter =  new ChatAdapter(activity);
        lstChatList = (ListView)fragment.findViewById(R.id.lstChatList);
        lstChatList.setAdapter(mChatAdapter);

        txvGroupName = (TextView)fragment.findViewById(R.id.txvGroupName);
        txvGroupName.setText(Commons.groupName);

        imvBack = (ImageView)fragment.findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        edtMessage = (EditText)fragment.findViewById(R.id.edtMessage);
        imvSend = (ImageView)fragment.findViewById(R.id.imvSend);
        imvSend.setOnClickListener(this);

        loadMessages(readCount);
        return fragment;
    }

    private void loadMessages(final int readCnt) {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<ChatModel> mChatModelList = new ArrayList<>();
                        JSONArray jsonChatModelList = response.getJSONArray(ReqConst.CHAT_LIST);

                        for (int i = 0; i < jsonChatModelList.length(); i++){

                            ChatModel mChatModel = new ChatModel();
                            JSONObject jsonChatModel = (JSONObject)jsonChatModelList.get(i);

                            mChatModel.setUserId(jsonChatModel.getInt(ReqConst.USER_ID));
                            mChatModel.setUserName(jsonChatModel.getString(ReqConst.USER_NAME));
                            mChatModel.setPhotoUrl(jsonChatModel.getString(ReqConst.PHOTO_URL));
                            mChatModel.setMessageContent(jsonChatModel.getString(ReqConst.MESSAGE_CONTENT));

                            mChatModelList.add(mChatModel);
                        }

                        mChatAdapter.addMessageArray(mChatModelList);

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetMessagesRequest req = new GetMessagesRequest(String.valueOf(Commons.groupId), String.valueOf(readCnt), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void sendMessage() {
        message = edtMessage.getText().toString().trim();
        if (message.length() == 0) return;

        ChatModel mChatModel =  new ChatModel();

        mChatModel.setUserId(Commons.user.getId());
        mChatModel.setUserName(Commons.user.getUserName());
        mChatModel.setPhotoUrl(Commons.user.getPhotoUrl());
        mChatModel.setMessageContent(message);

        mChatAdapter.addOneMessage(mChatModel);
        lstChatList.smoothScrollToPosition(mChatAdapter.getCount() - 1);

        edtMessage.setText("");

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };

        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        SendChatMessageRequest req = new SendChatMessageRequest(String.valueOf(Commons.user.getId()),String.valueOf(Commons.groupId), message, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Commons.isChatFragment = false;
    }

    public ChatFragment getInstance(){
        return this;
    }

    public void receiveMessage(String json){
        try {

            JSONObject response = new JSONObject(json);
            ChatModel mChatModel =  new ChatModel();

            mChatModel.setUserId(response.getInt(ReqConst.USER_ID));
            mChatModel.setUserName(response.getString(ReqConst.USER_NAME));
            mChatModel.setPhotoUrl(response.getString(ReqConst.PHOTO_URL));
            mChatModel.setMessageContent(response.getString(ReqConst.MESSAGE_CONTENT));

            if (mChatModel.getUserId() ==  Commons.user.getId()) return;

            mChatAdapter.addOneMessage(mChatModel);
            lstChatList.smoothScrollToPosition(mChatAdapter.getCount() - 1);

        }catch (JSONException e){}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                activity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.imvSend:
                sendMessage();
                break;
        }
    }
}
