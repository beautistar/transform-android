package com.sts.transform.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sts.transform.Base.BaseActivity;
import com.sts.transform.R;

public class ForgotPwdActivity extends BaseActivity {

    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);

        loadLayout();
    }

    private void loadLayout() {
        btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
    }

    private void gotoLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnBack:
                gotoLoginActivity();
                break;
        }
    }
}
