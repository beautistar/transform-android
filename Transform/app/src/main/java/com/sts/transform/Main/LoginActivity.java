package com.sts.transform.Main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sts.transform.API.LoginRequest;
import com.sts.transform.API.LoginWithSocialRequest;
import com.sts.transform.API.SaveTokenRequest;
import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Models.MacroPointModel;
import com.sts.transform.Models.PointModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.Models.UserPropertyModel;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener{

    Button btnForgotPwd, btnSignup, btnLogin, btnFacebook, btnLoginWithGoogle;
    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;

    EditText edtEmail, edtPassword;

    String email = "", password = "" , userName = "", token = "";
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.CAMERA, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};
    int userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkAllPermission();

        email = Preference.getInstance().getValue(this, PrefConst.EMAIL, "");
        password = Preference.getInstance().getValue(this, PrefConst.PASSWORD, "");

        loadLayout();

        initSocialLoginRequest();
    }

    private void loadLayout() {

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPassword = (EditText)findViewById(R.id.edtPassword);

        btnForgotPwd = (Button)findViewById(R.id.btnForgotPwd);
        btnForgotPwd.setOnClickListener(this);

        btnLoginWithGoogle =  (Button)findViewById(R.id.btnLoginWithGoogle);
        btnLoginWithGoogle.setOnClickListener(this);

        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnFacebook = (Button)findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(this);

        if (email.length() > 0){
            edtEmail.setText(email);
            edtPassword.setText(password);
            loginWithEmail();
        }
    }

    private void gotoForgotPwdActivity() {
        Intent intent = new Intent(this, ForgotPwdActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoSingupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveToken(final boolean gotoPersonalInfoActivity){

        token = FirebaseInstanceId.getInstance().getToken();
        if (token.length() == 0){
            showAlertDialog("Network error!");
            return;
        }

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        if (gotoPersonalInfoActivity) gotoPersonalInfoActivity(); else  gotoMainActivity();

                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SaveTokenRequest req = new SaveTokenRequest(String.valueOf(Commons.user.getId()), token, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, password);
        Preference.getInstance().put(this, PrefConst.FIREBASE_TOKEN, token);
        Commons.user.setPassword(password);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoPersonalInfoActivity() {

        Intent intent = new Intent(this, PersonalInfoActivity.class);
        intent.putExtra(ReqConst.ID, userId);
        startActivity(intent);
        finish();
    }

    private void initSocialLoginRequest() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.sts.asopaotv",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void loginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, ReqConst.CODE_GOOGLE_LOGIN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            getUserInfo(acct);

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    private void getUserInfo(GoogleSignInAccount acct) {

        String firstName = "", lastName = "";
        try{
            firstName = acct.getDisplayName().split(" ")[0];
            lastName = acct.getDisplayName().split(" ")[1];
        } catch (Exception e){ }

        userName = firstName +" " + lastName;
        email = acct.getEmail();
        password = acct.getId();

        registerUserInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ReqConst.CODE_GOOGLE_LOGIN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google Connect Fail", "Connection Failed");
    }

    private void registerUserInfo() {

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setUserName(jsonUser.getString(ReqConst.USER_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPassword(password);
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        JSONObject jsonPropertyModel = response.getJSONObject(ReqConst.PROPERTY_MODEL);
                        UserPropertyModel userPropertyModel = new UserPropertyModel();
                        userPropertyModel.setGender(jsonPropertyModel.getString(ReqConst.GENDER));
                        userPropertyModel.setBirthday(jsonPropertyModel.getString(ReqConst.BIRTHDAY));
                        userPropertyModel.setActivityFactor(jsonPropertyModel.getString(ReqConst.ACTIVITY_FACTOR));
                        userPropertyModel.setAge(jsonPropertyModel.getString(ReqConst.AGE));
                        userPropertyModel.setHeight(jsonPropertyModel.getString(ReqConst.HEIGHT));
                        userPropertyModel.setBodyWeight(jsonPropertyModel.getString(ReqConst.BODY_WEIGHT));
                        userPropertyModel.setAverageWaist(jsonPropertyModel.getString(ReqConst.AVERAGE_WAIST));
                        userPropertyModel.setAverageWrist(jsonPropertyModel.getString(ReqConst.AVERAGE_WRIST));
                        userPropertyModel.setAverageHip(jsonPropertyModel.getString(ReqConst.AVERAGE_HIP));
                        userPropertyModel.setAverageAbdomen(jsonPropertyModel.getString(ReqConst.AVERAGE_ABDOMEN));
                        userPropertyModel.setStartingBodyWeight(jsonPropertyModel.getString(ReqConst.STARTING_BODY_WEIGHT));

                        userModel.setUserPropertyModel(userPropertyModel);

                        MacroPointModel mMacroPointModel = new MacroPointModel();
                        JSONObject jsonMacroPointModel = response.getJSONObject(ReqConst.MACRO_MODEL);
                        mMacroPointModel.setBodyFat(jsonMacroPointModel.getString(ReqConst.BODY_FAT));
                        mMacroPointModel.setBodyMass(jsonMacroPointModel.getString(ReqConst.BODY_Mass));
                        mMacroPointModel.setProtein(jsonMacroPointModel.getString(ReqConst.DAILY_Protein));
                        mMacroPointModel.setCarb(jsonMacroPointModel.getString(ReqConst.DAILY_Carb));
                        mMacroPointModel.setFat(jsonMacroPointModel.getString(ReqConst.DAILY_Fat));
                        mMacroPointModel.setTotalBlocks(jsonMacroPointModel.getString(ReqConst.TOTAL_Blocks));
                        mMacroPointModel.setEatBlocksTillNow(jsonMacroPointModel.getString(ReqConst.EAT_BLOCKS_TILL_NOW));
                        mMacroPointModel.setStartingBodyFatPercent(jsonMacroPointModel.getString(ReqConst.STARTING_BODY_FAT_PERCENT));

                        userModel.setMacroPointModel(mMacroPointModel);

                        ArrayList<PointModel> mPointModelList = new ArrayList<>();
                        JSONArray jsonPointModelList = response.getJSONArray(ReqConst.POINT_MODEL_LIST);
                        for (int i =  0; i < jsonPointModelList.length(); i ++){
                            PointModel mPointModel =  new PointModel();
                            JSONObject jsonPointModel = (JSONObject) jsonPointModelList.get(i);
                            mPointModel.setGroupId(jsonPointModel.getInt(ReqConst.GROUP_ID));
                            mPointModel.setTodayPoints(jsonPointModel.getInt(ReqConst.TODAY_POINTS));
                            mPointModel.setTotalPoints(jsonPointModel.getInt(ReqConst.TOTAL_POINTS));

                            mPointModelList.add(mPointModel);
                        }

                        userModel.setPointModels(mPointModelList);

                        Commons.user = userModel;

                        saveToken(false);

                    }else if (resultCode == ReqConst.CODE_FIRST_LOGIN_WITH_SOCIAL_ACCOUNT){

                        userId = response.getInt(ReqConst.USER_ID);

                        UserModel userModel = new UserModel();
                        userModel.setId(userId);
                        userModel.setUserName(userName);
                        userModel.setEmail(email);
                        userModel.setPassword(password);
                        Commons.user = userModel;

                        saveToken(true);

                    } else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginWithSocialRequest req = new LoginWithSocialRequest(userName , email, password, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        try {
                                            // Application code
                                            email = object.getString("email");

                                            String fullName = object.getString("name");
                                            String firstName = fullName.split(" ")[0];  String lastName = fullName.split(" ")[1];
                                            userName = firstName + " " + lastName;
                                            password = object.getString("id");

                                            registerUserInfo();

                                        }catch (Exception e){}
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean valid() {

        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();

        if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        }else if (password.length() == 0){
            showAlertDialog("Please input password");
            return false;
        }

        return true;
    }

    private void loginWithEmail() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONObject jsonUser = response.getJSONObject(ReqConst.USER_MODEL);
                        UserModel userModel = new UserModel();
                        userModel.setId(jsonUser.getInt(ReqConst.ID));
                        userModel.setUserName(jsonUser.getString(ReqConst.USER_NAME));
                        userModel.setEmail(jsonUser.getString(ReqConst.EMAIL));
                        userModel.setPassword(password);
                        userModel.setPhotoUrl(jsonUser.getString(ReqConst.PHOTO_URL));

                        UserPropertyModel userPropertyModel = new UserPropertyModel();
                        JSONObject jsonPropertyModel = response.getJSONObject(ReqConst.PROPERTY_MODEL);
                        userPropertyModel.setGender(jsonPropertyModel.getString(ReqConst.GENDER));
                        userPropertyModel.setBirthday(jsonPropertyModel.getString(ReqConst.BIRTHDAY));
                        userPropertyModel.setActivityFactor(jsonPropertyModel.getString(ReqConst.ACTIVITY_FACTOR));
                        userPropertyModel.setAge(jsonPropertyModel.getString(ReqConst.AGE));
                        userPropertyModel.setHeight(jsonPropertyModel.getString(ReqConst.HEIGHT));
                        userPropertyModel.setBodyWeight(jsonPropertyModel.getString(ReqConst.BODY_WEIGHT));
                        userPropertyModel.setAverageWaist(jsonPropertyModel.getString(ReqConst.AVERAGE_WAIST));
                        userPropertyModel.setAverageWrist(jsonPropertyModel.getString(ReqConst.AVERAGE_WRIST));
                        userPropertyModel.setAverageHip(jsonPropertyModel.getString(ReqConst.AVERAGE_HIP));
                        userPropertyModel.setAverageAbdomen(jsonPropertyModel.getString(ReqConst.AVERAGE_ABDOMEN));
                        userPropertyModel.setStartingBodyWeight(jsonPropertyModel.getString(ReqConst.STARTING_BODY_WEIGHT));

                        userModel.setUserPropertyModel(userPropertyModel);

                        MacroPointModel mMacroPointModel = new MacroPointModel();
                        JSONObject jsonMacroPointModel = response.getJSONObject(ReqConst.MACRO_MODEL);
                        mMacroPointModel.setBodyFat(jsonMacroPointModel.getString(ReqConst.BODY_FAT));
                        mMacroPointModel.setBodyMass(jsonMacroPointModel.getString(ReqConst.BODY_Mass));
                        mMacroPointModel.setProtein(jsonMacroPointModel.getString(ReqConst.DAILY_Protein));
                        mMacroPointModel.setCarb(jsonMacroPointModel.getString(ReqConst.DAILY_Carb));
                        mMacroPointModel.setFat(jsonMacroPointModel.getString(ReqConst.DAILY_Fat));
                        mMacroPointModel.setTotalBlocks(jsonMacroPointModel.getString(ReqConst.TOTAL_Blocks));
                        mMacroPointModel.setEatBlocksTillNow(jsonMacroPointModel.getString(ReqConst.EAT_BLOCKS_TILL_NOW));
                        mMacroPointModel.setStartingBodyFatPercent(jsonMacroPointModel.getString(ReqConst.STARTING_BODY_FAT_PERCENT));

                        userModel.setMacroPointModel(mMacroPointModel);

                        ArrayList<PointModel> mPointModelList = new ArrayList<>();
                        JSONArray jsonPointModelList = response.getJSONArray(ReqConst.POINT_MODEL_LIST);
                        for (int i =  0; i < jsonPointModelList.length(); i ++){
                            PointModel mPointModel =  new PointModel();
                            JSONObject jsonPointModel = (JSONObject) jsonPointModelList.get(i);
                            mPointModel.setGroupId(jsonPointModel.getInt(ReqConst.GROUP_ID));
                            mPointModel.setTodayPoints(jsonPointModel.getInt(ReqConst.TODAY_POINTS));
                            mPointModel.setTotalPoints(jsonPointModel.getInt(ReqConst.TOTAL_POINTS));

                            mPointModelList.add(mPointModel);
                        }

                        userModel.setPointModels(mPointModelList);

                        Commons.user = userModel;

                        saveToken(false);
                    }else {
                        showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginRequest req = new LoginRequest(email, password, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }
    //==================== Permission========================================
    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {}
    }
//==================================================================

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnForgotPwd:
                gotoForgotPwdActivity();
                break;
            case R.id.btnSignup:
                gotoSingupActivity();
                break;
            case R.id.btnLogin:
                if (valid()){
                    loginWithEmail();
                }
                break;
            case R.id.btnLoginWithGoogle:
                loginWithGoogle();
                break;
            case R.id.btnFacebook:
                loginWithFacebook();
                break;
        }
    }
}
