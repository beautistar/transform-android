package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import java.util.ArrayList;

public class PrevChallengeListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<GroupModel> allData = new ArrayList<>();

    public PrevChallengeListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<GroupModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public GroupModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PrevChallengeListAdapter.CustomHolder holder;
        if (convertView == null){

            holder = new PrevChallengeListAdapter.CustomHolder();

            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_prev_challenge, parent, false);

            holder.setId(convertView);
            convertView.setTag(holder);
        }else{
            holder = (PrevChallengeListAdapter.CustomHolder)convertView.getTag();
        }

        GroupModel model = allData.get(position);
        holder.setData(model);

        return convertView;
    }

    private class CustomHolder{
        TextView txvGroupName;

        private void setId (View view){
            txvGroupName = (TextView)view.findViewById(R.id.txvGroupName);
        }

        private void setData(GroupModel model){
            txvGroupName.setText(model.getGroupName());
        }
    }
}
