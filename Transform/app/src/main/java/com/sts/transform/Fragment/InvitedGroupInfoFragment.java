package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sts.transform.API.GetInvitedUserListRequest;
import com.sts.transform.API.GetUserInfoByNameRequest;
import com.sts.transform.API.JoinGroupRequest;
import com.sts.transform.API.PaymentProcessRequest;
import com.sts.transform.Adapter.InvitedUserListAdapter;
import com.sts.transform.Adapter.PointParameterAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import net.authorize.acceptsdk.AcceptSDKApiClient;
import net.authorize.acceptsdk.datamodel.merchant.ClientKeyBasedMerchantAuthentication;
import net.authorize.acceptsdk.datamodel.transaction.CardData;
import net.authorize.acceptsdk.datamodel.transaction.EncryptTransactionObject;
import net.authorize.acceptsdk.datamodel.transaction.TransactionObject;
import net.authorize.acceptsdk.datamodel.transaction.TransactionType;
import net.authorize.acceptsdk.datamodel.transaction.callbacks.EncryptTransactionCallback;
import net.authorize.acceptsdk.datamodel.transaction.response.EncryptTransactionResponse;
import net.authorize.acceptsdk.datamodel.transaction.response.ErrorTransactionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class InvitedGroupInfoFragment extends BaseFragment implements EncryptTransactionCallback {

    MainActivity activity;
    PointParameterAdapter mPointParameterAdapter;
    InvitedUserListAdapter mInvitedUserListAdapter;

    TextView txvGroupContent, txvTypeOfChallenge, txvStartDate, txvCost, txvEndDate, txvGroupDescription;
    ListView lstInvitedUserList;
    ExpandableHeightListView lstPointParameters;
    Button btnJoinGroup, btnMaybeLater;
    EditText edtCardNumber, edtMonth, edtYear, edtCVV;
    Button btnPay;

    GroupModel invitedGroupInfo;

    private String cardNumber;
    private String month;
    private String year;
    private String cvv;
    private final String CLIENT_KEY = "26x856Xfg6JprywPfz3kbxZDc8q727B4R9s7BJ86q4ccL2C3DGWQw88qZLJgsQXY";
    private final String API_LOGIN_ID = "5nu7PFT82B6"; // replace with your API LOGIN_ID
    private AcceptSDKApiClient apiClient;

    String price = "";

    public InvitedGroupInfoFragment(MainActivity activity) {
        this.activity = activity;
        this.invitedGroupInfo = Commons.invitedGroupInfo;

        price =  invitedGroupInfo.getPriceOfGroup();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            apiClient = new AcceptSDKApiClient.Builder(getActivity(),
                    AcceptSDKApiClient.Environment.PRODUCTION).connectionTimeout(
                    4000) // optional connection time out in milliseconds
                    .build();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_invited_group_info, container, false);

        txvGroupDescription = (TextView)fragment.findViewById(R.id.txvGroupDescription);

        btnMaybeLater = (Button)fragment.findViewById(R.id.btnMaybeLater);
        btnMaybeLater.setOnClickListener(this);

        txvGroupContent = (TextView)fragment.findViewById(R.id.txvGroupContent);
        txvTypeOfChallenge = (TextView)fragment.findViewById(R.id.txvTypeOfChallenge);
        txvStartDate = (TextView)fragment.findViewById(R.id.txvStartDate);
        txvCost = (TextView)fragment.findViewById(R.id.txvCost);
        txvEndDate = (TextView)fragment.findViewById(R.id.txvEndDate);

        mPointParameterAdapter = new PointParameterAdapter(activity);
        lstPointParameters = (ExpandableHeightListView) fragment.findViewById(R.id.lstPointParameters);
        lstPointParameters.setAdapter(mPointParameterAdapter);
        lstPointParameters.setExpanded(true);

        mInvitedUserListAdapter = new InvitedUserListAdapter(activity);
        lstInvitedUserList = (ListView)fragment.findViewById(R.id.lstInvitedUserList);
        lstInvitedUserList.setAdapter(mInvitedUserListAdapter);

        btnJoinGroup = (Button)fragment.findViewById(R.id.btnJoinGroup);
        btnJoinGroup.setOnClickListener(this);

        setData();

        return fragment;
    }

    private void setData() {
        txvGroupContent.setText("You are invited to " + invitedGroupInfo.getGroupName() +  " nutrition challenge, created by " + invitedGroupInfo.getCreatorName());
        txvTypeOfChallenge.setText(invitedGroupInfo.getGroupType());
        switch (invitedGroupInfo.getGroupType()){
            case Constants.GROUP_GENERAL_TYPE:
                txvGroupDescription.setText(getString(R.string.general_content));
                break;
            case Constants.GROUP_MACRO_TYPE:
                txvGroupDescription.setText(getString(R.string.macro_content));
                break;
            case Constants.GROUP_PALEO_TYPE:
                txvGroupDescription.setText(getString(R.string.paleo_content));
                break;
            case Constants.GROUP_KETO_TYPE:
                txvGroupDescription.setText(getString(R.string.keto_content));
                break;
        }


        mPointParameterAdapter.refresh(invitedGroupInfo.getPointParameter().split("_"));
        txvStartDate.setText(invitedGroupInfo.getStartDate());
        txvCost.setText(invitedGroupInfo.getPriceOfGroup());
        txvEndDate.setText(invitedGroupInfo.getEndDate());

        getInvitedUserList();
    }

    private void getInvitedUserList() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    activity.closeProgress();
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<UserModel> mUserModelList = new ArrayList<>();
                        JSONArray jsonUserModelList = response.getJSONArray(ReqConst.USER_LIST);

                        for (int i = 0; i < jsonUserModelList.length(); i ++){

                            UserModel mUserModel = new UserModel();
                            JSONObject jsonUserModel = (JSONObject)jsonUserModelList.get(i);

                            mUserModel.setId(jsonUserModel.getInt(ReqConst.ID));
                            mUserModel.setUserName(jsonUserModel.getString(ReqConst.USER_NAME));

                            mUserModelList.add(mUserModel);
                        }
                        mInvitedUserListAdapter.refresh(mUserModelList);
                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        GetInvitedUserListRequest req = new GetInvitedUserListRequest(String .valueOf(invitedGroupInfo.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void joinGroup() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    activity.closeProgress();
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        activity.showAlertDialog("Congratulations on joining the group Challenge, " +
                                invitedGroupInfo.getGroupName() + "!\nYou will now see the point parameters on your home page. Make sure you start logging your points starting on " +
                                invitedGroupInfo.getStartDate() );

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        JoinGroupRequest req = new JoinGroupRequest(String.valueOf(Commons.user.getId()), String.valueOf(invitedGroupInfo.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    protected void inputCardInfo(){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_input_card_info);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        edtCardNumber =  (EditText)dialog.findViewById(R.id.edtCardNumber);
        setUpCreditCardEditText();
        edtMonth = (EditText)dialog.findViewById(R.id.edtMonth);
        edtYear = (EditText)dialog.findViewById(R.id.edtYear);
        edtCVV = (EditText)dialog.findViewById(R.id.edtCVV);

        btnPay = (Button)dialog.findViewById(R.id.btnPay);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cardNumber = edtCardNumber.getText().toString().trim().replace(" ", "");
                month = edtMonth.getText().toString().trim();
                year = edtYear.getText().toString().trim();
                cvv = edtCVV.getText().toString().trim();

                if ((cardNumber != null && cardNumber.isEmpty()) || (month != null && month.isEmpty()) || (year != null && year.isEmpty()) || (cvv != null && cvv.isEmpty())){
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                }

                if (cardNumber.length() < Constants.MIN_CARD_NUMBER_LENGTH){
                    edtCardNumber.requestFocus();
                    edtCardNumber.setError(activity.getString(R.string.invalid_card_number));
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                    return;
                }

                int monthNum = Integer.parseInt(month);
                if (monthNum < 1 || monthNum > 12){
                    edtMonth.requestFocus();
                    edtMonth.setError(getString(R.string.invalid_month));
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                    return;
                }

                if (month.length() < Constants.MIN_YEAR_LENGTH){
                    edtMonth.requestFocus();
                    edtMonth.setError(getString(R.string.two_digit_month));
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                    return;
                }

                year = Constants.YEAR_PREFIX + edtYear.getText().toString();
                if (year.length() < Constants.MIN_YEAR_LENGTH){
                    edtYear.requestFocus();
                    edtYear.setError(getString(R.string.invalid_year));
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                    return;
                }

                cvv = edtCVV.getText().toString().trim();
                if (cvv.length() < Constants.MIN_CVV_LENGTH){
                    edtCVV.requestFocus();
                    edtCVV.setError(getString(R.string.invalid_cvv));
                    btnPay.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
                    return;
                }

                dialog.dismiss();

                getPaymentToken();
            }
        });

        dialog.show();
    }

    private void getPaymentToken() {

        activity.showProgress();

        try {
            EncryptTransactionObject transactionObject = prepareTransactionObject();
            apiClient.getTokenWithRequest(transactionObject, this);
        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            activity.closeProgress();
            e.printStackTrace();
        }
    }

    private boolean isEmptyField() {
        return (cardNumber != null && cardNumber.isEmpty()) || (month != null && month.isEmpty()) || (
                year != null
                        && year.isEmpty()) || (cvv != null && cvv.isEmpty());
    }

    private EncryptTransactionObject prepareTransactionObject() {
        ClientKeyBasedMerchantAuthentication merchantAuthentication =
                ClientKeyBasedMerchantAuthentication.
                        createMerchantAuthentication(API_LOGIN_ID, CLIENT_KEY);

        return TransactionObject.
                createTransactionObject(
                        TransactionType.SDK_TRANSACTION_ENCRYPTION) // type of transaction object
                .cardData(prepareCardDataFromFields()) // card data to get Token
                .merchantAuthentication(merchantAuthentication).build();
    }

    private CardData prepareCardDataFromFields() {
        return new CardData.Builder(cardNumber, month, year).cvvCode(cvv) //CVV Code is optional
                .build();
    }

    @Override
    public void onErrorReceived(ErrorTransactionResponse error) {
        activity.hideSoftKeyboard();
        activity.closeProgress();
        activity.showBottomToast(error.getFirstErrorMessage().getMessageText());
    }

    @Override
    public void onEncryptionFinished(EncryptTransactionResponse response) {
        activity.hideSoftKeyboard();
        activity.closeProgress();
        paymentProcess();
    }

    private void paymentProcess() {
        String cardNumber = this.cardNumber;
        String expirationDate = this.year + "-" + this.month;
        String  cardCode = this.cvv;

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        joinGroup();

                    }else {

                        activity.showAlertDialog(response.getString("ErrorMessage"));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        PaymentProcessRequest req = new PaymentProcessRequest(String.valueOf(Commons.user.getId()), String.valueOf(invitedGroupInfo.getId()),
                Commons.user.getUserName(), Commons.user.getEmail(), cardNumber, expirationDate, cardCode, invitedGroupInfo.getPriceOfGroup() , res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void setUpCreditCardEditText() {
        edtCardNumber.addTextChangedListener(new TextWatcher() {
            private boolean spaceDeleted;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // check if a space was deleted
                CharSequence charDeleted = s.subSequence(start, start + count);
                spaceDeleted = " ".equals(charDeleted.toString());
            }

            public void afterTextChanged(Editable editable) {
                // disable text watcher
                edtCardNumber.removeTextChangedListener(this);

                // record cursor position as setting the text in the textview
                // places the cursor at the end
                int cursorPosition = edtCardNumber.getSelectionStart();
                String withSpaces = formatText(editable);
                edtCardNumber.setText(withSpaces);
                // set the cursor at the last position + the spaces added since the
                // space are always added before the cursor
                edtCardNumber.setSelection(cursorPosition + (withSpaces.length() - editable.length()));

                // if a space was deleted also deleted just move the cursor
                // before the space
                if (spaceDeleted) {
                    edtCardNumber.setSelection(edtCardNumber.getSelectionStart() - 1);
                    spaceDeleted = false;
                }

                // enable text watcher
                edtCardNumber.addTextChangedListener(this);
            }

            private String formatText(CharSequence text) {
                StringBuilder formatted = new StringBuilder();
                int count = 0;
                for (int i = 0; i < text.length(); ++i) {
                    if (Character.isDigit(text.charAt(i))) {
                        if (count % 4 == 0 && count > 0) formatted.append(" ");
                        formatted.append(text.charAt(i));
                        ++count;
                    }
                }
                return formatted.toString();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnJoinGroup:
                inputCardInfo();
//                joinGroup();
                break;
            case R.id.btnMaybeLater:
                activity.gotoJoinGroupFragment();
                break;
        }
    }
}
