package com.sts.transform.Chat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/28/2017.
 */

public class ChatAdapter extends BaseAdapter {


    private final int ME = 1, FRIEND = 2;
    MainActivity activity;

    ArrayList<ChatModel> allData =  new ArrayList<>();

    public ChatAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void addOneMessage(ChatModel oneMessage){
        allData.add(oneMessage);
        notifyDataSetChanged();
    }

    public void addMessageArray(ArrayList<ChatModel> messageArray){
        allData.clear();
        allData.addAll(messageArray);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public ChatModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){

            holder = new CustomHolder();
            LayoutInflater  inflater = LayoutInflater.from(activity);

            if (getItem(position).getUserId() == Commons.user.getId()){
                convertView = inflater.inflate(R.layout.item_chat_me, parent, false);
            }else {
                convertView = inflater.inflate(R.layout.item_chat_friend, parent, false);
            }

            holder.imvPhoto = (RoundedImageView)convertView.findViewById(R.id.imvPhoto);
            holder.txvUserName = (TextView)convertView.findViewById(R.id.txvUserName);
            holder.txvMessage = (TextView)convertView.findViewById(R.id.txvMessage);

            convertView.setTag(holder);
        }else {
            holder =  (CustomHolder)convertView.getTag();
        }

        ChatModel model = allData.get(position);
        if (model.getPhotoUrl().length() > 0){
            Picasso.with(activity).load(model.getPhotoUrl()).into(holder.imvPhoto);
        }

        holder.txvUserName.setText(model.getUserName());
        holder.txvMessage.setText(model.getMessageContent());

        return convertView;
    }

    private class CustomHolder{

        RoundedImageView imvPhoto;
        TextView txvUserName, txvMessage;
    }
}
