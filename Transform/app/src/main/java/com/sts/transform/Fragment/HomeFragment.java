package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sts.transform.API.AddPointRequest;
import com.sts.transform.API.GetGroupInfoRequest;
import com.sts.transform.API.GetPrevChallengeRequest;
import com.sts.transform.API.GetRankingListInGroupRequest;
import com.sts.transform.API.GiveBenchmarkRequest;
import com.sts.transform.Adapter.GroupListAdapter;
import com.sts.transform.Adapter.GroupMemberListAdapter;
import com.sts.transform.Adapter.PointParameterInHomeFragmentAdapter;
import com.sts.transform.Adapter.RankingListAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.PointModel;
import com.sts.transform.Models.RankingModel;
import com.sts.transform.Models.UserModel;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends BaseFragment {

    MainActivity activity;
    PointParameterInHomeFragmentAdapter mPointParameterInHomeFragmentAdapter;
    GroupMemberListAdapter mGroupMemberListAdapter;

    LinearLayout lytGroupName, lytEatBlocks;
    TextView txvGroupName, txvEatProtein, txvEatCarbs, txvEatFat, txvRemainProtein, txvRemainCarbs, txvRemainFat, txvCompletedPercent;
    TextView txvGroupType, txvGroupKind, txvStartingBodyWeight, txvStartingBodyFatPercent, txvCurrentPlacing;
    public TextView txvTodayPoints, txvTotalPoints;
    ImageView imvGroupChat, imvGroupRanking;
    ExpandableHeightListView lstPointParameterList;
    Button btnSubmitBlock;
    LinearLayout lytSelectMember;

    public int selectedGroupId = 0;
    String selectedGroupName = "";
    ArrayList<GroupModel> mGroupModelList = new ArrayList<>();
    ArrayList<GroupModel> mPrevChallengeList = new ArrayList<>();
    public static GroupModel selectedGroup = new GroupModel();
    int eatBlocksTillNow = 0, remainBlocks = 0;

    public int todayPoints = 0, totalPoints = 0;
    ArrayList<RankingModel> mRankingModelList = new ArrayList<>();
    ArrayList<UserModel> mUserModelList;

    public int userIdForBenchmark = 0;
    boolean existUserForBenchmark = false, clickedGroupRanking = false;

    public HomeFragment(MainActivity activity) {

        this.activity = activity;

        selectedGroupId = Preference.getInstance().getValue(activity, PrefConst.SELECTED_GROUP_ID, 0);
        mGroupModelList.clear();
        mPrevChallengeList.clear();
        userIdForBenchmark = 0;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_home, container, false);

        lytSelectMember = (LinearLayout) fragment.findViewById(R.id.lytSelectMember);
        lytSelectMember.setOnClickListener(this);

        txvCurrentPlacing = (TextView) fragment.findViewById(R.id.txvCurrentPlacing);

        txvStartingBodyWeight = (TextView) fragment.findViewById(R.id.txvStartingBodyWeight);
        txvStartingBodyWeight.setText(Commons.user.getUserPropertyModel().getStartingBodyWeight());

        txvStartingBodyFatPercent = (TextView) fragment.findViewById(R.id.txvStartingBodyFatPercent);
        txvStartingBodyFatPercent.setText(Commons.user.getMacroPointModel().getStartingBodyFatPercent());

        imvGroupRanking = (ImageView) fragment.findViewById(R.id.imvGroupRanking);
        imvGroupRanking.setOnClickListener(this);

        txvGroupKind = (TextView) fragment.findViewById(R.id.txvGroupKind);
        txvGroupType = (TextView) fragment.findViewById(R.id.txvGroupType);

        txvTodayPoints = (TextView) fragment.findViewById(R.id.txvTodayPoints);
        txvTotalPoints = (TextView) fragment.findViewById(R.id.txvTotalPoints);

        txvCompletedPercent = (TextView) fragment.findViewById(R.id.txvCompletedPercent);
        lytEatBlocks = (LinearLayout) fragment.findViewById(R.id.lytEatBlocks);
        txvEatProtein = (TextView) fragment.findViewById(R.id.txvEatProtein);
        txvEatCarbs = (TextView) fragment.findViewById(R.id.txvEatCarbs);
        txvEatFat = (TextView) fragment.findViewById(R.id.txvEatFat);
        txvRemainProtein = (TextView) fragment.findViewById(R.id.txvRemainProtein);
        txvRemainCarbs = (TextView) fragment.findViewById(R.id.txvRemainCarbs);
        txvRemainFat = (TextView) fragment.findViewById(R.id.txvRemainFat);

        btnSubmitBlock = (Button) fragment.findViewById(R.id.btnSubmitBlock);
        btnSubmitBlock.setOnClickListener(this);

        mPointParameterInHomeFragmentAdapter = new PointParameterInHomeFragmentAdapter(activity, this);
        lstPointParameterList = (ExpandableHeightListView) fragment.findViewById(R.id.lstPointParameterList);
        lstPointParameterList.setAdapter(mPointParameterInHomeFragmentAdapter);
        lstPointParameterList.setExpanded(true);

        lytGroupName = (LinearLayout) fragment.findViewById(R.id.lytGroupName);
        lytGroupName.setOnClickListener(this);

        txvGroupName = (TextView) fragment.findViewById(R.id.txvGroupName);

        imvGroupChat = (ImageView) fragment.findViewById(R.id.imvGroupChat);
        imvGroupChat.setOnClickListener(this);

        return fragment;
    }

    private void getGroupInfo() {
        mGroupModelList.clear();
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try {
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS) {

                        JSONArray jsonGroupModelList = response.getJSONArray(ReqConst.GROUP_LIST);

                        for (int i = 0; i < jsonGroupModelList.length(); i++) {

                            GroupModel mGroupModel = new GroupModel();
                            JSONObject jsonGroupModel = (JSONObject) jsonGroupModelList.get(i);

                            mGroupModel.setId(jsonGroupModel.getInt(ReqConst.ID));
                            mGroupModel.setCreatorId(jsonGroupModel.getInt(ReqConst.CREATOR_ID));
                            mGroupModel.setKindOfGroup(jsonGroupModel.getString(ReqConst.KIND_OF_GROUP));
                            mGroupModel.setGroupName(jsonGroupModel.getString(ReqConst.GROUP_NAME));
                            mGroupModel.setGroupType(jsonGroupModel.getString(ReqConst.GROUP_TYPE));
                            mGroupModel.setPointParameter(jsonGroupModel.getString(ReqConst.POINT_PARAMETER));
                            mGroupModel.setPriceOfGroup(jsonGroupModel.getString(ReqConst.PRICE));
                            mGroupModel.setStartDate(jsonGroupModel.getString(ReqConst.START_DATE));
                            mGroupModel.setEndDate(jsonGroupModel.getString(ReqConst.END_DATE));
                            mGroupModel.setCreatorName(jsonGroupModel.getString(ReqConst.CREATOR_NAME));
                            mGroupModel.setChallengeFinished(jsonGroupModel.getString(ReqConst.CHALLENGE_FINISHED));

                            mGroupModelList.add(mGroupModel);
                        }


                    } else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                } catch (JSONException e) {
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetGroupInfoRequest req = new GetGroupInfoRequest(String.valueOf(Commons.user.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void getPrevChallengeList() {
        mPrevChallengeList.clear();
        Commons.prevChallengeList.clear();
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try {
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS) {

                        JSONArray jsonGroupModelList = response.getJSONArray(ReqConst.GROUP_LIST);

                        for (int i = 0; i < jsonGroupModelList.length(); i++) {

                            GroupModel mGroupModel = new GroupModel();
                            JSONObject jsonGroupModel = (JSONObject) jsonGroupModelList.get(i);

                            mGroupModel.setId(jsonGroupModel.getInt(ReqConst.ID));
                            mGroupModel.setCreatorId(jsonGroupModel.getInt(ReqConst.CREATOR_ID));
                            mGroupModel.setKindOfGroup(jsonGroupModel.getString(ReqConst.KIND_OF_GROUP));
                            mGroupModel.setGroupName(jsonGroupModel.getString(ReqConst.GROUP_NAME));
                            mGroupModel.setGroupType(jsonGroupModel.getString(ReqConst.GROUP_TYPE));
                            mGroupModel.setPointParameter(jsonGroupModel.getString(ReqConst.POINT_PARAMETER));
                            mGroupModel.setPriceOfGroup(jsonGroupModel.getString(ReqConst.PRICE));
                            mGroupModel.setStartDate(jsonGroupModel.getString(ReqConst.START_DATE));
                            mGroupModel.setEndDate(jsonGroupModel.getString(ReqConst.END_DATE));
                            mGroupModel.setCreatorName(jsonGroupModel.getString(ReqConst.CREATOR_NAME));
                            mGroupModel.setChallengeFinished(jsonGroupModel.getString(ReqConst.CHALLENGE_FINISHED));

                            mPrevChallengeList.add(mGroupModel);
                        }

                        Commons.prevChallengeList = mPrevChallengeList;

                    } else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                } catch (JSONException e) {
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetPrevChallengeRequest req = new GetPrevChallengeRequest(String.valueOf(Commons.user.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void getSelectedGroupInfo() {

        mPointParameterInHomeFragmentAdapter = new PointParameterInHomeFragmentAdapter(activity, this);
        lstPointParameterList.setAdapter(mPointParameterInHomeFragmentAdapter);

        boolean flag = false;
        if (mGroupModelList.size() == 0) {
            lytSelectMember.setVisibility(View.GONE);
            activity.showBottomToast("No any group");
            return;
        }

        if (selectedGroupId == 0) {
            selectedGroupId = mGroupModelList.get(0).getId();
            selectedGroup = mGroupModelList.get(0);
            selectedGroupName = mGroupModelList.get(0).getGroupName();

            flag = true;

        } else {
            for (int i = 0; i < mGroupModelList.size(); i++) {
                if (selectedGroupId == mGroupModelList.get(i).getId()) {
                    selectedGroup = mGroupModelList.get(i);
                    selectedGroupName = mGroupModelList.get(i).getGroupName();

                    flag = true;
                }
            }
        }

        if (flag) {
            if (selectedGroup.getCreatorId() == Commons.user.getId()) {
                txvGroupKind.setText("My Group");
            } else {
                txvGroupKind.setText(selectedGroup.getKindOfGroup());
            }

            txvGroupName.setText(selectedGroup.getGroupName());
            txvGroupType.setText(selectedGroup.getGroupType());

            Preference.getInstance().put(activity, PrefConst.SELECTED_GROUP_ID, selectedGroupId);

            ArrayList<String> arrayList = new ArrayList<>();

            if (selectedGroup.getGroupType().equals(Constants.GROUP_PALEO_TYPE)) {
                arrayList.add(Constants.PALEO_BASE_POINT);
            } else if (selectedGroup.getGroupType().equals(Constants.GROUP_KETO_TYPE)) {
                arrayList.add(Constants.KETO_BASE_POINT);
            } else if (selectedGroup.getGroupType().equals(Constants.GROUP_GENERAL_TYPE)) {
                arrayList.add(Constants.GENERAL_BASE_POINT);
            }

            String temp[] = selectedGroup.getPointParameter().split("_");
            for (int p = 0; p < temp.length; p++) {
                arrayList.add(temp[p]);
            }

            mPointParameterInHomeFragmentAdapter.refresh(arrayList);

            getPointsInGroup();
        } else {
            activity.showBottomToast("No any group");
        }

        if (selectedGroup.getCreatorId() == Commons.user.getId()) {
            lytSelectMember.setVisibility(View.VISIBLE);
        } else {
            lytSelectMember.setVisibility(View.GONE);
        }

        if (selectedGroup.getChallengeFinished().equals("1")) {
            activity.showAlertDialog("the " + selectedGroup.getGroupName() + " has finished! Congratulations to the winners, prizes will be awarded. Keep up your diet and join a new group with us today!");
        }

        getGroupRanking();
    }

    private void getPointsInGroup() {
        boolean flag = false;
        for (int i = 0; i < Commons.user.getPointModels().size(); i++) {
            if (selectedGroupId == Commons.user.getPointModels().get(i).getGroupId()) {
                todayPoints = Commons.user.getPointModels().get(i).getTodayPoints();
                totalPoints = Commons.user.getPointModels().get(i).getTotalPoints();

                txvTotalPoints.setText(String.valueOf(totalPoints));
                txvTodayPoints.setText(String.valueOf(todayPoints));

                flag = true;
            }
        }

        if (!flag) {
            txvTotalPoints.setText("");
            txvTodayPoints.setText("");
            totalPoints = 0;
            todayPoints = 0;
        }
    }

    private void showGroupList() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_group_list);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final GroupListAdapter mGroupListAdapter = new GroupListAdapter(activity);
        ListView lstGroupList = (ListView) dialog.findViewById(R.id.lstGroupList);
        lstGroupList.setAdapter(mGroupListAdapter);
        mGroupListAdapter.refresh(mGroupModelList);

        lstGroupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedGroupId = mGroupListAdapter.getItem(position).getId();
                getSelectedGroupInfo();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getEatBlocksTillNow() {
        eatBlocksTillNow = Preference.getInstance().getValue(activity, PrefConst.eatBlockTillNow, 0);

        if (Commons.user.getMacroPointModel().getTotalBlocks().length() == 0) {
            remainBlocks = 0;
        } else {
            remainBlocks = Integer.valueOf(Commons.user.getMacroPointModel().getTotalBlocks()) - eatBlocksTillNow;
        }

        lytEatBlocks.setVisibility(View.VISIBLE);
        txvEatProtein.setText(String.valueOf(eatBlocksTillNow * 7));
        txvEatCarbs.setText(String.valueOf(eatBlocksTillNow * 9));
        txvEatFat.setText(String.valueOf(eatBlocksTillNow * 1.5));

        txvRemainProtein.setText(String.valueOf(remainBlocks * 7));
        txvRemainCarbs.setText(String.valueOf(remainBlocks * 9));
        txvRemainFat.setText(String.valueOf(remainBlocks * 1.5));

        if (Commons.user.getMacroPointModel().getTotalBlocks().equals("0") || Commons.user.getMacroPointModel().getTotalBlocks().length() == 0) {
            txvCompletedPercent.setText("0");
        } else {
            txvCompletedPercent.setText(String.valueOf(100 * eatBlocksTillNow / Integer.valueOf(Commons.user.getMacroPointModel().getTotalBlocks())));
        }
    }

    private void showSubmitBlockDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_submit_new_block);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText edtRemainBlock = (EditText)dialog.findViewById(R.id.edtRemainBlock);
        if (Commons.user.getMacroPointModel().getTotalBlocks().length() > 0){
            edtRemainBlock.setText(String.valueOf(Integer.valueOf(Commons.user.getMacroPointModel().getTotalBlocks()) - eatBlocksTillNow));
        }

        final EditText edtEatBlocksToday = (EditText)dialog.findViewById(R.id.edtEatBlocksToday);

        Button btnSubmit = (Button)dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eatBlocksToday = edtEatBlocksToday.getText().toString().trim();
                if (eatBlocksToday.length() > 0){
                    eatBlocksTillNow = eatBlocksTillNow + Integer.valueOf(eatBlocksToday);
                }

                submitBlock(eatBlocksTillNow);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void submitBlock(int tempEatBlocksTillNow){
        eatBlocksTillNow = tempEatBlocksTillNow;
        remainBlocks =  Integer.valueOf(Commons.user.getMacroPointModel().getTotalBlocks()) -  eatBlocksTillNow;

        lytEatBlocks.setVisibility(View.VISIBLE);
        txvEatProtein.setText(String.valueOf(eatBlocksTillNow * 7));
        txvEatCarbs.setText(String.valueOf(eatBlocksTillNow * 9));
        txvEatFat.setText(String.valueOf(eatBlocksTillNow * 1.5));

        txvRemainProtein.setText(String.valueOf(remainBlocks * 7));
        txvRemainCarbs.setText(String.valueOf(remainBlocks * 9));
        txvRemainFat.setText(String.valueOf(remainBlocks * 1.5));

        if (Commons.user.getMacroPointModel().getTotalBlocks().equals("0") || Commons.user.getMacroPointModel().getTotalBlocks().length() == 0){
            txvCompletedPercent.setText("0");
        }else {
            txvCompletedPercent.setText(String.valueOf(100 * eatBlocksTillNow/Integer.valueOf(Commons.user.getMacroPointModel().getTotalBlocks())));
        }

        if (remainBlocks == 0 && selectedGroup.getGroupType().equals(Constants.GROUP_MACRO_TYPE)){
            addPoint5("");
        }

        Preference.getInstance().put(activity, PrefConst.eatBlockTillNow, eatBlocksTillNow);


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String dd = df.format(c);

        dd = (dd.split("-"))[0];
        Preference.getInstance().put(activity, PrefConst.DATE_FOR_BLOCK, dd);
    }


    public void addPoint5(final String checkedPoint) {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){
                        todayPoints = todayPoints + 5;
                        totalPoints = totalPoints + 5;

                        txvTodayPoints.setText(String.valueOf(todayPoints));
                        txvTotalPoints.setText(String.valueOf(totalPoints));

                        for (int i = 0; i < Commons.user.getPointModels().size(); i++){
                            if (Commons.user.getPointModels().get(i).getGroupId() == selectedGroupId){
                                Commons.user.getPointModels().get(i).setTotalPoints(totalPoints);
                                Commons.user.getPointModels().get(i).setTodayPoints(todayPoints);
                            }
                        }

                        saveCheckedPoint(checkedPoint);

                        activity.refreshUserInfo();

                    }else {
                        activity.showAlertDialog(activity.getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(activity.getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(activity.getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        AddPointRequest req = new AddPointRequest(String.valueOf(Commons.user.getId()), String.valueOf(selectedGroup.getId()), String.valueOf(5), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void getGroupRanking() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        mRankingModelList.clear();
                        JSONArray jsonRankingModelList = response.getJSONArray(ReqConst.RANKING_LIST);

                        mUserModelList =  new ArrayList<>();

                        for (int i = 0; i < jsonRankingModelList.length(); i++){

                            UserModel mUserModel = new UserModel();

                            RankingModel mRankingModel = new RankingModel();
                            JSONObject jsonRankingModel = (JSONObject) jsonRankingModelList.get(i);

                            mRankingModel.setUserId(jsonRankingModel.getInt(ReqConst.USER_ID));
                            mRankingModel.setTotalPoints(jsonRankingModel.getInt(ReqConst.TOTAL_POINTS));
                            mRankingModel.setUserName(jsonRankingModel.getString(ReqConst.USER_NAME));
                            mRankingModel.setPhotoUrl(jsonRankingModel.getString(ReqConst.PHOTO_URL));

                            mUserModel.setId(jsonRankingModel.getInt(ReqConst.USER_ID));
                            mUserModel.setUserName(jsonRankingModel.getString(ReqConst.USER_NAME));
                            mUserModel.setBenchmark(jsonRankingModel.getInt(ReqConst.BENCHMARK));
                            mUserModel.setPhotoUrl(jsonRankingModel.getString(ReqConst.PHOTO_URL));
                            mUserModel.setTotalPointOfSelectedGroup(jsonRankingModel.getInt(ReqConst.TOTAL_POINTS));

                            if (mUserModel.getBenchmark() == 1){
                                userIdForBenchmark = mUserModel.getId();
                                mPointParameterInHomeFragmentAdapter.hasBenchmark();
                                existUserForBenchmark = true;
                            }

                            mUserModelList.add(mUserModel);

                            mRankingModelList.add(mRankingModel);
                        }

                        Collections.sort(mRankingModelList, new Comparator<RankingModel>() {
                            @Override
                            public int compare(RankingModel o1, RankingModel o2) {

                                return Integer.valueOf(o2.getTotalPoints()).compareTo(Integer.valueOf(o1.getTotalPoints()));
                            }
                        });

                        txvCurrentPlacing.setText("");
                        for (int i = 0; i < mRankingModelList.size(); i ++){
                            if (mRankingModelList.get(i).getUserId() ==  Commons.user.getId()){
                                txvCurrentPlacing.setText(String.valueOf(i + 1));
                            }
                        }

                        Commons.rankingModelList = mRankingModelList;

                        if (clickedGroupRanking){
                            showRankingList();
                            clickedGroupRanking = false;
                        }

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetRankingListInGroupRequest req = new GetRankingListInGroupRequest(String.valueOf(selectedGroup.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }



    private void showRankingList() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ranking_list);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        RankingListAdapter mRankingListAdapter = new RankingListAdapter(activity);
        ListView lstRankingList = (ListView)dialog.findViewById(R.id.lstRankingList);
        lstRankingList.setAdapter(mRankingListAdapter);
        mRankingListAdapter.refresh(mRankingModelList);

        dialog.show();
    }

    private void showGiveBenchmarkDialog() {

        if (mUserModelList == null) return;

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_give_benchmark);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvNoMembers = (TextView)dialog.findViewById(R.id.txvNoMembers);
        Button btnGiveBenchmark = (Button)dialog.findViewById(R.id.btnGiveBenchmark);
        btnGiveBenchmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (existUserForBenchmark)return;

                if (userIdForBenchmark == 0){
                    activity.showBottomToast("Please select one member");
                    return;
                }

                giveBenchmark();
                dialog.dismiss();
            }
        });

        Button btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        mGroupMemberListAdapter = new GroupMemberListAdapter(activity);
        final ListView lstMemberList = (ListView)dialog.findViewById(R.id.lstMemberList);
        lstMemberList.setAdapter(mGroupMemberListAdapter);

        lstMemberList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                if (existUserForBenchmark)return;

                for (int i = 0; i < lstMemberList.getChildCount(); i ++){
                    if (position != i){
                        ((ImageView)lstMemberList.getChildAt(i).findViewById(R.id.imvCheck)).setVisibility(View.GONE);
                    }else {
                        ((ImageView)lstMemberList.getChildAt(i).findViewById(R.id.imvCheck)).setVisibility(View.VISIBLE);
                        userIdForBenchmark = mGroupMemberListAdapter.getItem(position).getId();
                    }
                }
            }
        });

        if (mUserModelList.size() == 0){
            lstMemberList.setVisibility(View.GONE);
            btnGiveBenchmark.setVisibility(View.GONE);

            txvNoMembers.setVisibility(View.VISIBLE);
        }else {
            lstMemberList.setVisibility(View.VISIBLE);
            btnGiveBenchmark.setVisibility(View.VISIBLE);

            txvNoMembers.setVisibility(View.GONE);
            mGroupMemberListAdapter.refresh(mUserModelList);
        }

        dialog.show();
    }

    public void giveBenchmark(){

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        activity.showBottomToast("Success!");
                        mPointParameterInHomeFragmentAdapter.hasBenchmark();

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GiveBenchmarkRequest req = new GiveBenchmarkRequest(String.valueOf(selectedGroupId), String.valueOf(userIdForBenchmark), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void saveCheckedPoint(String checkedPoint) {
        switch (checkedPoint){
            case Constants.pointContent1:
                Preference.getInstance().put(activity, PrefConst.POINT1, "1");
                break;
            case Constants.pointContent2:
                Preference.getInstance().put(activity, PrefConst.POINT2, "1");
                break;
            case Constants.pointContent3:
                Preference.getInstance().put(activity, PrefConst.POINT3, "1");
                break;
            case Constants.pointContent4:
                Preference.getInstance().put(activity, PrefConst.POINT4, "1");
                break;
            case Constants.pointContent5:
                Preference.getInstance().put(activity, PrefConst.POINT5, "1");
                break;
            case Constants.pointContent6:
                Preference.getInstance().put(activity, PrefConst.POINT6, "1");
                break;
            case Constants.pointContent7:
                Preference.getInstance().put(activity, PrefConst.POINT7, "1");
                break;
            case Constants.pointContent8:
                Preference.getInstance().put(activity, PrefConst.POINT8, "1");
                break;
            case Constants.pointContent9:
                Preference.getInstance().put(activity, PrefConst.POINT9, "1");
                break;
            case Constants.pointContent10:
                Preference.getInstance().put(activity, PrefConst.POINT10, "1");
                break;
            case Constants.pointContent11:
                Preference.getInstance().put(activity, PrefConst.POINT11, "1");
                break;
            case Constants.pointContent12:
                Preference.getInstance().put(activity, PrefConst.POINT12, "1");
                break;
            case Constants.pointContent13:
                Preference.getInstance().put(activity, PrefConst.POINT13, "1");
                break;
            case Constants.pointContent14:
                Preference.getInstance().put(activity, PrefConst.POINT14, "1");
                break;
            case Constants.pointContent15:
                Preference.getInstance().put(activity, PrefConst.POINT15, "1");
                break;
        }

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String dd = df.format(c);

        dd = (dd.split("-"))[0];

        Preference.getInstance().put(activity, PrefConst.DATE_FOR_POINT, dd);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!activity.completedProfile) completeEditProfile();

        getGroupInfo();
        getEatBlocksTillNow();
        getPrevChallengeList();
    }

    public void completeEditProfile() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_complete_edit_profile);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button btnOk =  (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytGroupName:
                showGroupList();
                break;
            case R.id.imvGroupChat:
                Commons.groupId = selectedGroupId;
                Commons.groupName = selectedGroupName;
                activity.gotoChatFragment();
                break;
            case R.id.btnSubmitBlock:
                if (selectedGroup.getChallengeFinished().equals("1")) return;

                activity.checkProfile();
                if (activity.completedProfile && remainBlocks != 0){
                    showSubmitBlockDialog();
                }
                if (!activity.completedProfile){
                    completeEditProfile();
                }else if (remainBlocks == 0){
                    activity.showBottomToast("No needed block");
                }

                break;
            case R.id.imvGroupRanking:
                getGroupRanking();
                clickedGroupRanking = true;
                break;
            case R.id.lytSelectMember:
                showGiveBenchmarkDialog();
                break;
        }
    }
}

