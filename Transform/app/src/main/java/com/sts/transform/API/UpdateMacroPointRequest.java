package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class UpdateMacroPointRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_UPDATE_MACRO_POINT;
    private Map<String,String> params;

    public UpdateMacroPointRequest(String userId, String bodyFat, String bodyMass, String dailyProtein, String dailyCarb,
            String dailyFat, String totalBlocks, String startingBodyFatPercent, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
        params.put(ReqConst.BODY_FAT, bodyFat);
        params.put(ReqConst.BODY_Mass, bodyMass);
        params.put(ReqConst.DAILY_Protein, dailyProtein);
        params.put(ReqConst.DAILY_Carb, dailyCarb);
        params.put(ReqConst.DAILY_Fat, dailyFat);
        params.put(ReqConst.TOTAL_Blocks, totalBlocks);
        params.put(ReqConst.STARTING_BODY_FAT_PERCENT, startingBodyFatPercent);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
