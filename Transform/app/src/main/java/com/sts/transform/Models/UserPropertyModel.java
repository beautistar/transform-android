package com.sts.transform.Models;

import java.io.Serializable;

/**
 * Created by STS on 11/18/2017.
 */

public class UserPropertyModel implements Serializable {

    String gender = "", birthday = "", activityFactor = "", age = "", height = "";;
    String  bodyWeight = "", averageWrist = "",  averageWaist = "", averageHip = "", averageAbdomen = "";
    String startingBodyWeight = "";

    public String getStartingBodyWeight() {
        return startingBodyWeight;
    }

    public void setStartingBodyWeight(String startingBodyWeight) {
        this.startingBodyWeight = startingBodyWeight;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getActivityFactor() {
        return activityFactor;
    }

    public void setActivityFactor(String activityFactor) {
        this.activityFactor = activityFactor;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getBodyWeight() {
        return bodyWeight;
    }

    public void setBodyWeight(String bodyWeight) {
        this.bodyWeight = bodyWeight;
    }

    public String getAverageWrist() {
        return averageWrist;
    }

    public void setAverageWrist(String averageWrist) {
        this.averageWrist = averageWrist;
    }

    public String getAverageWaist() {
        return averageWaist;
    }

    public void setAverageWaist(String averageWaist) {
        this.averageWaist = averageWaist;
    }

    public String getAverageHip() {
        return averageHip;
    }

    public void setAverageHip(String averageHip) {
        this.averageHip = averageHip;
    }

    public String getAverageAbdomen() {
        return averageAbdomen;
    }

    public void setAverageAbdomen(String averageAbdomen) {
        this.averageAbdomen = averageAbdomen;
    }
}
