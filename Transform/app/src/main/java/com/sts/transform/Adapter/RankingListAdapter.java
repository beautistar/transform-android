package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.RankingModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/9/2017.
 */

public class RankingListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<RankingModel> allData = new ArrayList<>();

    public RankingListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<RankingModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public RankingModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomHolder holder;
        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_ranking, parent, false);

            holder.setId(convertView);

            RankingModel model = allData.get(position);
            holder.setData(model, position);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class  CustomHolder implements View.OnClickListener{

        TextView txvRanking, txvUserName, txvTotalPoints;
        RoundedImageView imvPhoto;
        LinearLayout lytContainer;

        private void setId(View view){
            txvRanking = (TextView)view.findViewById(R.id.txvRanking);
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            txvTotalPoints = (TextView)view.findViewById(R.id.txvTotalPoints);
            imvPhoto = (RoundedImageView)view.findViewById(R.id.imvPhoto);
            lytContainer = (LinearLayout)view.findViewById(R.id.lytContainer);
        }

        private void setData(RankingModel model, int position){
            txvRanking.setText(String.valueOf(position + 1));
            txvUserName.setText(model.getUserName());
            txvTotalPoints.setText(String.valueOf(model.getTotalPoints()));

            if (model.getPhotoUrl().length() > 0){
                Picasso.with(activity).load(model.getPhotoUrl()).into(imvPhoto);
            }

            if (model.getUserId() ==  Commons.user.getId()){
                lytContainer.setBackgroundResource(R.drawable.bg_unfill_red_border_rect);
            }
        }

        @Override
        public void onClick(View v) {}
    }
}
