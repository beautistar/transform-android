package com.sts.transform.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sts.transform.Base.BaseActivity;
import com.sts.transform.Commons.Constants;
import com.sts.transform.R;

public class LibraryDetailActivity extends BaseActivity {

    WebView webView;

    String fileUrl =  "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_detail);

        fileUrl = getIntent().getStringExtra(Constants.FILE_RUL);

        Log.d("fileUrl====", fileUrl);

        loadLayout();
    }

    private void loadLayout() {
        webView =  (WebView)findViewById(R.id.webView);
        if (fileUrl.contains(".pdf")){
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);
            webView.setInitialScale(100);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webView.setWebViewClient(new WebViewClient());
            String url =  "http://docs.google.com/gview?embedded=true&url=" + fileUrl;
            webView.loadUrl(url);
        }else {
            webView.loadUrl(fileUrl);
        }
    }
}
