package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sts.transform.Commons.Commons;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/9/2017.
 */

public class GroupListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<GroupModel> allData = new ArrayList<>();

    public GroupListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<GroupModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public GroupModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomHolder holder;
        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_group_list, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        GroupModel model = allData.get(position);
        holder.setData(model);

        return convertView;
    }

    private class  CustomHolder implements View.OnClickListener{

        TextView txvInvitation, txvType, txvGroupKind,txvMyGroup;

        private void setId(View view){
            txvInvitation =  (TextView)view.findViewById(R.id.txvInvitation);
            txvType = (TextView)view.findViewById(R.id.txvType);
            txvGroupKind = (TextView)view.findViewById(R.id.txvGroupKind);
            txvMyGroup = (TextView)view.findViewById(R.id.txvMyGroup);
        }

        private void setData(GroupModel model){

            txvInvitation.setText(model.getGroupName());

            if (model.getCreatorId() == Commons.user.getId()){
                txvMyGroup.setVisibility(View.VISIBLE);
            }else {
                txvMyGroup.setVisibility(View.GONE);
            }

            txvType.setText(model.getGroupType());

            if (model.getCreatorId() == Commons.user.getId()){
                txvGroupKind.setText("My Group");
            }else {
                txvGroupKind.setText(model.getKindOfGroup());
            }
        }

        @Override
        public void onClick(View v) {}
    }
}
