package com.sts.transform.Preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String FIREBASE_TOKEN = "token";
    public static final String SELECTED_GROUP_ID = "selectedGroupId";
    public static final String DATE_FOR_POINT = "date_time";
    public static final String DATE_FOR_BLOCK = "date_time_block";


    public static final String POINT1 = "point1";
    public static final String POINT2 = "point2";
    public static final String POINT3 = "point3";
    public static final String POINT4 = "point4";
    public static final String POINT5 = "point5";
    public static final String POINT6 = "point6";
    public static final String POINT7 = "point7";
    public static final String POINT8 = "point8";
    public static final String POINT9 = "point9";
    public static final String POINT10 = "point10";
    public static final String POINT11 = "point11";
    public static final String POINT12 = "point12";
    public static final String POINT13 = "point13";
    public static final String POINT14 = "point14";
    public static final String POINT15 = "point15";

    public static final String eatBlockTillNow = "eatBlocTillNow";
}
