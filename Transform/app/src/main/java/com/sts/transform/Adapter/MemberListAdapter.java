package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.MemberModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 1/10/2018.
 */

public class MemberListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<MemberModel> allData =  new ArrayList<>();

    public MemberListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void clearMemberList(){
        allData.clear();
    }
    public void addMember (MemberModel data){
        allData.add(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public MemberModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup viewGroup) {

        CustomHolder holder;
        if (itemView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater =  LayoutInflater.from(activity);
            itemView = inflater.inflate(R.layout.item_member, viewGroup, false);

            holder.setId(itemView);
            holder.setData(getItem(position));

            itemView.setTag(holder);
        }else {
            holder =  (CustomHolder)itemView.getTag();
        }

        return itemView;
    }

    private class CustomHolder {

        TextView txvUserName, txvEmail, txvPhoneNumber;

        private void setId(View view){
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            txvEmail = (TextView)view.findViewById(R.id.txvEmail);
            txvPhoneNumber = (TextView)view.findViewById(R.id.txvPhoneNumber);
        }

        private void setData(MemberModel model){
            txvUserName.setText(model.getName());
            txvEmail.setText(model.getEmail());
            txvPhoneNumber.setText(model.getPhoneNumber());
        }
    }
}
