package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.GetAllPublicGroupInfoRequest;
import com.sts.transform.API.GetInvitedGroupInfoRequest;
import com.sts.transform.Adapter.GroupMemberListAdapter;
import com.sts.transform.Adapter.OpenGroupListAdapter;
import com.sts.transform.Adapter.InvitationAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class JoinGroupFragment extends BaseFragment {

    MainActivity activity;
    InvitationAdapter mInvitationAdapter;
    OpenGroupListAdapter mOpenGroupListAdapter;

    ListView lstInvitationList, lstPublicGroupList;

    public JoinGroupFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_join_group, container, false);

        mInvitationAdapter = new InvitationAdapter(activity);
        lstInvitationList = (ListView)fragment.findViewById(R.id.lstInvitationList);
        lstInvitationList.setAdapter(mInvitationAdapter);
        lstInvitationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.invitedGroupInfo = mInvitationAdapter.getItem(position);
                activity.gotoInvitedGroupInfoFragment();
            }
        });

        mOpenGroupListAdapter = new OpenGroupListAdapter(activity);
        lstPublicGroupList = (ListView)fragment.findViewById(R.id.lstPublicGroupList);
        lstPublicGroupList.setAdapter(mOpenGroupListAdapter);
        lstPublicGroupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Commons.invitedGroupInfo = mOpenGroupListAdapter.getItem(position);
                activity.gotoInvitedGroupInfoFragment();
            }
        });


        getAllOpenGroupInfo();

        return fragment;
    }

    private void getAllOpenGroupInfo() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<GroupModel> mGroupModelList = new ArrayList<>();
                        JSONArray jsonGroupModelList = response.getJSONArray(ReqConst.GROUP_LIST);

                        for (int i = 0; i < jsonGroupModelList.length(); i ++){

                            GroupModel mGroupModel = new GroupModel();
                            JSONObject jsonGroupModel = (JSONObject)jsonGroupModelList.get(i);

                            mGroupModel.setId(jsonGroupModel.getInt(ReqConst.ID));
                            mGroupModel.setCreatorId(jsonGroupModel.getInt(ReqConst.CREATOR_ID));
                            mGroupModel.setKindOfGroup(jsonGroupModel.getString(ReqConst.KIND_OF_GROUP));
                            mGroupModel.setGroupName(jsonGroupModel.getString(ReqConst.GROUP_NAME));
                            mGroupModel.setGroupType(jsonGroupModel.getString(ReqConst.GROUP_TYPE));
                            mGroupModel.setPointParameter(jsonGroupModel.getString(ReqConst.POINT_PARAMETER));
                            mGroupModel.setPriceOfGroup(jsonGroupModel.getString(ReqConst.PRICE));
                            mGroupModel.setStartDate(jsonGroupModel.getString(ReqConst.START_DATE));
                            mGroupModel.setEndDate(jsonGroupModel.getString(ReqConst.END_DATE));
                            mGroupModel.setCreatorName(jsonGroupModel.getString(ReqConst.CREATOR_NAME));

                            mGroupModelList.add(mGroupModel);
                        }

                        mOpenGroupListAdapter.refresh(mGroupModelList);

                        getAllInvitedGroup();

                    }else {
                        activity.closeProgress();
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetAllPublicGroupInfoRequest req = new GetAllPublicGroupInfoRequest(String.valueOf(Commons.user.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void getAllInvitedGroup() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    activity.closeProgress();
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        ArrayList<GroupModel> mGroupModelList = new ArrayList<>();
                        JSONArray jsonGroupModelList = response.getJSONArray(ReqConst.GROUP_LIST);

                        for (int i = 0; i < jsonGroupModelList.length(); i ++){

                            GroupModel mGroupModel = new GroupModel();
                            JSONObject jsonGroupModel = (JSONObject)jsonGroupModelList.get(i);

                            mGroupModel.setId(jsonGroupModel.getInt(ReqConst.ID));
                            mGroupModel.setCreatorId(jsonGroupModel.getInt(ReqConst.CREATOR_ID));
                            mGroupModel.setKindOfGroup(jsonGroupModel.getString(ReqConst.KIND_OF_GROUP));
                            mGroupModel.setGroupName(jsonGroupModel.getString(ReqConst.GROUP_NAME));
                            mGroupModel.setGroupType(jsonGroupModel.getString(ReqConst.GROUP_TYPE));
                            mGroupModel.setPointParameter(jsonGroupModel.getString(ReqConst.POINT_PARAMETER));
                            mGroupModel.setPriceOfGroup(jsonGroupModel.getString(ReqConst.PRICE));
                            mGroupModel.setStartDate(jsonGroupModel.getString(ReqConst.START_DATE));
                            mGroupModel.setEndDate(jsonGroupModel.getString(ReqConst.END_DATE));
                            mGroupModel.setCreatorName(jsonGroupModel.getString(ReqConst.CREATOR_NAME));

                            mGroupModelList.add(mGroupModel);
                        }
                        mInvitationAdapter.refresh(mGroupModelList);
                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        GetInvitedGroupInfoRequest req = new GetInvitedGroupInfoRequest(String .valueOf(Commons.user.getId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }
}
