package com.sts.transform.Commons;

import javax.xml.parsers.SAXParser;

/**
 * Created by STS on 10/27/2017.
 */

public class ReqConst {

    public static final String SERVER_URL = "http://18.216.154.87/index.php/api/";
    public static final String RES_CODE = "resultCode";

    public static final int CODE_SUCCESS = 100;
    public static final int CODE_GOOGLE_LOGIN = 10;
    public static final int CODE_ALREADY_REGISTERED = 103;
    public static final int CODE_FIRST_LOGIN_WITH_SOCIAL_ACCOUNT = 104;
    public static final int CODE_NO_EAT_BLOCK = 105;

    /// Sign up
    public static final String  FUN_SIGNUP = "signup";
    public static final String  USER_NAME = "userName";
    public static final String  EMAIL = "email";
    public static final String  PASSWORD = "password";
    public static final String  USER_MODEL = "userModel";
    public static final String ID = "id";
    public static final String USER_ID = "userId";

    /// login with social account;
    public static final String FUN_LOGIN_WITH_SOCIAL = "loginWithSocial";

    // login with email
    public static final String FUN_LOGIN = "login";
    public static final String PROPERTY_MODEL = "propertyModel";
    public static final String MACRO_MODEL = "macroModel";

    /// upload personal info
    public static final String FUN_UPLOAD_PERSONAL_INFO = "uploadPersonalInfo";
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    public static final String AGE = "age";
    public static final String BODY_WEIGHT = "bodyWeight";
    public static final String HEIGHT = "height";
    public static final String ACTIVITY_FACTOR = "activityFactor";
    public static final String AVERAGE_WRIST = "wristMeasurement";
    public static final String AVERAGE_WAIST = "waistMeasurement";
    public static final String AVERAGE_HIP = "averageHip";
    public static final String AVERAGE_ABDOMEN = "averageAbdomen";
    public static final String PROTEIN = "protein";
    public static final String CARB = "carb";
    public static final String FAT = "fat";
    public static final String STARTING_BODY_WEIGHT = "startingBodyWeight";

    /// create Group
    public static final String FUN_CREATE_GROUP = "createGroup";
    public static final String GROUP_NAME = "groupName";
    public static final String KIND_OF_GROUP = "kindOfGroup";
    public static final String GROUP_TYPE = "groupType";
    public static final String POINT_PARAMETER = "pointParameter";
    public static final String PRICE = "price";
    public static final String CREATOR_NAME = "creatorName";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String CREATOR_ID = "creatorId";
    public static final String CHALLENGE_FINISHED = "challengeFinished";

    /// get user info by name
    public static final String FUN_GET_USER_INFO_BY_NAME = "getUserInfoByName";
    public static final String USER_MODEL_LIST = "userModelList";

    /// save token
    public static final String FUN_SAVE_TOKEN  = "saveToken";
    public static final String TOKEN = "token";

    /// sendPushForInvite
    public static final String FUN_SEND_PUSH_FOR_INVITE = "sendPushForInvite";


    // get all open group info
    public static final String FUN_GET_ALL_PUBLIC_GROUP_INFO = "getAllPublicGroupInfo";
    public static final String GROUP_LIST = "groupList";
    public static final String GROUP_ID = "groupId";

    // get invited group info
    public static final String FUN_GET_INVITED_GROUP_INFO = "getInvitedGroupInfo";

    // get invited user list ;
    public static final String FUN_GET_INVITED_USER_LIST = "getUserListInGroup";
    public static final String USER_LIST = "userList";

    // join group
    public static final String FUN_JOIN_GROUP = "joinGroup";

    // get prev messages
    public static final String FUN_GET_MESSAGES = "getMessages";
    public static final String READ_COUNT = "readCount";
    public static final String CHAT_LIST = "chatList";
    public static final String PHOTO_URL = "photoUrl";
    public static final String MESSAGE_CONTENT = "messageContent";

    /// get chat message;
    public static final String CHAT_MODEL = "chatModel";

    /// send message;
    public static final String FUN_SEND_MESSAGE = "sendMessage";

    // get group info
    public static final String FUN_GET_ALL_GROUP_LIST = "getAllGroupList";

    // get group info
    public static final String FUN_GET_PREV_CHALLENGE_LIST = "getPreviousChallenges";

    /// get macro history
    public static final String FUN_GET_EAT_BLOCKS_TILL_NOW = "getEatBlocksTillNow";

    public static final String FUN_UPDATE_MACRO_POINT = "updateMacroPoint";
    public static final String BODY_FAT = "bodyFat";
    public static final String BODY_Mass = "bodyMass";
    public static final String DAILY_Protein = "dailyProtein";
    public static final String DAILY_Carb = "dailyCarb";
    public static final String DAILY_Fat = "dailyFat";
    public static final String TOTAL_Blocks = "totalBlocks";
    public static final String EAT_BLOCKS_TILL_NOW = "eatBlocksTillNow";
    public static final String STARTING_BODY_FAT_PERCENT = "startingBodyFatPercent";

    // save eat blocks
    public static final String SAVE_EAT_BLOCKS_TILL_NOW = "saveEatBlocksTillNow";

    // add today point
    public static final String FUN_ADD_POINT = "addPoint";
    public static final String TODAY_POINTS = "todayPoints";
    public static final String TOTAL_POINTS = "totalPoints";
    public static final String POINT_MODEL_LIST = "pointModelList";
    public static final String POINT_CNT = "pointCnt";

    // delete point
    public static final String FUN_DELETE_POINT = "deletePoint";

    // upload Photo
    public static final String FUN_UPLOAD_PHOTO = "uploadPhoto";
    public static final String PARAM_FILE = "file";

    // get user in group
    public static final String FUN_GET_USER_IN_GROUP = "getRankingListInGroup";
    public static final String RANKING_LIST = "rankingList";

    // give benchmark
    public static final String FUN_GIVE_BENCH_MARK = "giveBenchmark";
    public static final String BENCHMARK = "benchmark";

    // payJoinFee
    public static final String FUN_PAYMENT_PROCESS = "paymentProcess";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String  EXPIRATION_DATA = "expirationDate";
    public static final String  CARD_CODE = "cardCode";
    public static final String AMOUNT = "amount";

    // get library data
    public static final String FUN_GET_LIBRARY_DATA =  "getLibrary";
    public static final String LIBRARY = "library";
    public static final String TITLE =  "title";
    public static final String DESCRIPTION = "description";
    public static final String PREVIEW_PICTURE = "preview_picture";
    public static final String FILE_URL = "file_url";

    // get library data
    public static final String FUN_GET_HOW_DATA =  "getHelp";
    public static final String HELP = "help";

}
