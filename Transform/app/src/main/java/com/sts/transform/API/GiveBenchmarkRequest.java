package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class GiveBenchmarkRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_GIVE_BENCH_MARK;
    private Map<String,String> params;

    public GiveBenchmarkRequest(String groupId, String userId , Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.GROUP_ID, groupId);
        params.put(ReqConst.USER_ID, userId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
