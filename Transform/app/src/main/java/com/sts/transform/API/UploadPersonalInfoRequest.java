package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class UploadPersonalInfoRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_PERSONAL_INFO;
    private Map<String,String> params;

    public UploadPersonalInfoRequest(String userId, String gender, String birthday, String age, String bodyWeight, String height, String activityFactor, String averageWrist, String averageWaist, String averageHip, String averageAbdomen
           , String startingBodyWeight , Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.ID, userId);
        params.put(ReqConst.GENDER, gender);
        params.put(ReqConst.BIRTHDAY, birthday);
        params.put(ReqConst.AGE, age);
        params.put(ReqConst.BODY_WEIGHT, bodyWeight);
        params.put(ReqConst.HEIGHT, height);
        params.put(ReqConst.ACTIVITY_FACTOR, activityFactor);
        params.put(ReqConst.AVERAGE_WRIST, averageWrist);
        params.put(ReqConst.AVERAGE_WAIST, averageWaist);
        params.put(ReqConst.AVERAGE_HIP, averageHip);
        params.put(ReqConst.AVERAGE_ABDOMEN, averageAbdomen);
        params.put(ReqConst.STARTING_BODY_WEIGHT, startingBodyWeight);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
