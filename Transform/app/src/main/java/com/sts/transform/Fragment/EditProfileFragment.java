package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.UploadPersonalInfoRequest;
import com.sts.transform.Adapter.TakeHeightWeightAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.UserPropertyModel;
import com.sts.transform.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class EditProfileFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    MainActivity activity;
    TakeHeightWeightAdapter mTakeHeightWeightAdapter;

    TextView txvGender, txvBirthday, txvAge, txvActivityFactor, txvWristHipTitle, txvWaistAbdomenTitle;
    Button btnSave;
    TextView txvHeight, txvBodyWeight, txvWristHip,txvWaistAbdomen;

    String gender = "", activityFactor = "", birthday = "", age = "", bodyWeight = "", wristMeasurement = "", waistMeasurement = "", height = "";
    String hipMeasurement = "", abdomenMeasurement = "";
        int year = 0, month = 0, day = 0, userId = 0;

    public EditProfileFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_edit, container, false);

        txvWristHipTitle = (TextView)fragment.findViewById(R.id.txvWristHipTitle);
        txvWaistAbdomenTitle = (TextView)fragment.findViewById(R.id.txvWaistAbdomenTitle);

        txvGender = (TextView)fragment.findViewById(R.id.txvGender);
        txvGender.setText(Commons.user.getUserPropertyModel().getGender());

        btnSave = (Button)fragment.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        txvBirthday = (TextView)fragment.findViewById(R.id.txvBirthday);
        txvBirthday.setText(Commons.user.getUserPropertyModel().getBirthday());

        txvAge = (TextView)fragment.findViewById(R.id.txvAge);
        txvAge.setText(String.valueOf(Commons.user.getUserPropertyModel().getAge()));

        txvBodyWeight = (TextView) fragment.findViewById(R.id.txvBodyWeight);
        txvBodyWeight.setText(String.valueOf(Commons.user.getUserPropertyModel().getBodyWeight()));
        txvBodyWeight.setOnClickListener(this);

        txvWristHip = (TextView) fragment.findViewById(R.id.txvWristHip);
        txvWristHip.setText(String.valueOf(Commons.user.getUserPropertyModel().getAverageWrist()));
        txvWristHip.setOnClickListener(this);

        txvWaistAbdomen = (TextView) fragment.findViewById(R.id.txvWaistAbdomen);
        txvWaistAbdomen.setText(String.valueOf(Commons.user.getUserPropertyModel().getAverageWaist()));
        txvWaistAbdomen.setOnClickListener(this);

        txvHeight = (TextView) fragment.findViewById(R.id.txvHeight);
        txvHeight.setText(String.valueOf(Commons.user.getUserPropertyModel().getHeight()));
        txvHeight.setOnClickListener(this);

        txvActivityFactor = (TextView)fragment.findViewById(R.id.txvActivityFactor);
        txvActivityFactor.setOnClickListener(this);
        txvActivityFactor.setText(Commons.user.getUserPropertyModel().getActivityFactor());

        return fragment;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        String tempMonth = "", tempDay = "", tempYear = String.valueOf(y);

        if (m < 10){
            tempMonth = "0" + (m+1);
        }else {
            tempMonth = String.valueOf(++m);
        }

        if (d < 10){
            tempDay = "0" + d;
        }else {
            tempDay = String.valueOf(d);
        }

        String date = tempMonth + "/" + tempDay + "/" + tempYear;
        txvBirthday.setText(date);
        txvAge.setText(String.valueOf(year - y));
    }

    private void selectActivityFactor() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_activity_factor);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final RadioButton rbSedentary =  (RadioButton)dialog.findViewById(R.id.rbSedentary);
        final RadioButton rbLight= (RadioButton)dialog.findViewById(R.id.rbLight);
        final RadioButton rbModerate= (RadioButton)dialog.findViewById(R.id.rbModerate);
        final RadioButton rbActivity = (RadioButton)dialog.findViewById(R.id.rbActivity);
        final RadioButton rbVeryActivity = (RadioButton)dialog.findViewById(R.id.rbVeryActivity);
        final RadioButton rbHeavyActivity = (RadioButton)dialog.findViewById(R.id.rbHeavyActivity);

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbSedentary.isChecked()){
                    activityFactor = Constants.SEDENTARY;
                }else if (rbLight.isChecked()){
                    activityFactor = Constants.LIGHT;
                }else if (rbModerate.isChecked()){
                    activityFactor = Constants.MODERATE;
                }else if (rbActivity.isChecked()){
                    activityFactor = Constants.ACTIVE;
                }else if (rbVeryActivity.isChecked()){
                    activityFactor = Constants.VERY_ACTIVE;
                }else if (rbHeavyActivity.isChecked()){
                    activityFactor = Constants.HEAVY_WEIGHT_TRAINING;
                }

                txvActivityFactor.setText(activityFactor);
                dialog.dismiss();
            }
        });


        dialog.show();

    }

    private boolean valid() {
        gender = txvGender.getText().toString().trim();
        birthday = txvBirthday.getText().toString().trim();
        age = txvAge.getText().toString().trim();
        bodyWeight = txvBodyWeight.getText().toString().trim();
        height = txvHeight.getText().toString().trim();
        activityFactor = txvActivityFactor.getText().toString().trim();

        if (gender.equals(Constants.MALE)){

            wristMeasurement = txvWristHip.getText().toString().trim();
            waistMeasurement = txvWaistAbdomen.getText().toString().trim();
        }else if (gender.equals(Constants.FEMALE)){
            hipMeasurement = txvWristHip.getText().toString().trim();
            abdomenMeasurement = txvWaistAbdomen.getText().toString().trim();
        }

        if (gender.length() == 0){
            activity.showAlertDialog("Please select gender");
            return false;
        }else if (birthday.length() == 0){
            activity.showAlertDialog("Please input birthday");
            return false;
        }else if (age.length() == 0){
            activity.showAlertDialog("Please input age");
            return false;
        }else if (wristMeasurement.length() == 0 && gender.equals(Constants.MALE)){
            activity.showAlertDialog("Please input wrist measurement");
            return false;
        }else if (waistMeasurement.length() == 0 && gender.equals(Constants.MALE)){
            activity.showAlertDialog("Please input waist measurement");
            return false;
        }else if (hipMeasurement.length() == 0 && gender.equals(Constants.FEMALE)){
            activity.showAlertDialog("Please input hip measurement");
            return false;
        }else if (abdomenMeasurement.length()== 0 && gender.equals(Constants.FEMALE)){
            activity.showAlertDialog("Please input abdomen measurement");
            return false;
        } else if (height.length() == 0){
            activity.showAlertDialog("Please input height");
            return false;
        }else if (activityFactor.length() == 0){
            activity.showAlertDialog("Please input activity factor");
            return false;
        }

        return true;
    }

    private void saveProfile() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        UserPropertyModel userPropertyModel =  new UserPropertyModel();
                        userPropertyModel.setGender(gender);
                        userPropertyModel.setBirthday(birthday);
                        userPropertyModel.setAge(age);
                        userPropertyModel.setBodyWeight(bodyWeight);
                        userPropertyModel.setAverageWrist(wristMeasurement);
                        userPropertyModel.setAverageWaist(waistMeasurement);
                        userPropertyModel.setHeight(height);
                        userPropertyModel.setActivityFactor(activityFactor);
                        userPropertyModel.setAverageHip(hipMeasurement);
                        userPropertyModel.setAverageAbdomen(abdomenMeasurement);
                        userPropertyModel.setStartingBodyWeight(Commons.user.getUserPropertyModel().getStartingBodyWeight());

                        Commons.user.setUserPropertyModel(userPropertyModel);

                        activity.showBottomToast("Success");
                        activity.gotoProfileFragment();

                        activity.completedProfile = true;

                        activity.refreshUserInfo();

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        UploadPersonalInfoRequest req = new UploadPersonalInfoRequest(String.valueOf(Commons.user.getId()), gender, birthday, String.valueOf(age), String.valueOf(bodyWeight), String.valueOf(height), activityFactor, wristMeasurement, waistMeasurement, hipMeasurement, abdomenMeasurement, Commons.user.getUserPropertyModel().getStartingBodyWeight(), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE)){

            txvWristHipTitle.setText("Wrist Measurement");
            txvWristHip.setText(Commons.user.getUserPropertyModel().getAverageWrist());

            txvWaistAbdomenTitle.setText("Waist Measurement");
            txvWaistAbdomen.setText(Commons.user.getUserPropertyModel().getAverageWaist());

        }else if (Commons.user.getUserPropertyModel().getGender().equals(Constants.FEMALE)){

            txvWristHipTitle.setText("Hip Measurement");
            txvWristHip.setText(Commons.user.getUserPropertyModel().getAverageHip());

            txvWaistAbdomenTitle.setText("Abdomen Measurement");
            txvWaistAbdomen.setText(Commons.user.getUserPropertyModel().getAverageAbdomen());
        }
    }

    private void takeHeight() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select height in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.heightList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvHeight.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeWeight() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Weight in lbs");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.weightList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvBodyWeight.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeWrist() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Wrist in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.wristList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvWristHip.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeWaist() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Waist in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.waistList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvWaistAbdomen.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeHip() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Hip in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.hipList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvWristHip.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeAbdomen() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_take_height);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvTitle = (TextView)dialog.findViewById(R.id.txvTitle);
        txvTitle.setText("Select Abdomen in inches");

        mTakeHeightWeightAdapter = new TakeHeightWeightAdapter(getContext());
        ListView lstHeightList = (ListView)dialog.findViewById(R.id.lstHeightList);
        lstHeightList.setAdapter(mTakeHeightWeightAdapter);
        mTakeHeightWeightAdapter.refresh(Commons.abdomenList);

        lstHeightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                txvWaistAbdomen.setText(mTakeHeightWeightAdapter.getItem(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvActivityFactor:
                selectActivityFactor();
                break;
            case R.id.btnSave:
                if (valid()){
                    saveProfile();
                }
                break;
            case R.id.txvHeight:
                takeHeight();
                break;
            case R.id.txvBodyWeight:
                takeWeight();
                break;
            case R.id.txvWristHip:
                if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE)){
                    takeWrist();
                }else if (Commons.user.getUserPropertyModel().getGender().equals(Constants.FEMALE)){
                    takeHip();
                }
                break;
            case R.id.txvWaistAbdomen:
                if (Commons.user.getUserPropertyModel().getGender().equals(Constants.MALE)){
                    takeWaist();
                }else if (Commons.user.getUserPropertyModel().getGender().equals(Constants.FEMALE)) {
                    takeAbdomen();
                }
                break;
        }
    }
}
