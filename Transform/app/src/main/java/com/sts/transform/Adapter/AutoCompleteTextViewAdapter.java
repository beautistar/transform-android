package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.UserModel;
import com.sts.transform.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/23/2017.
 */

public class AutoCompleteTextViewAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<UserModel> allData =  new ArrayList<>();

    public AutoCompleteTextViewAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<UserModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    public ArrayList<UserModel> getAllData(){
        return allData;
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder =  new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_auto_complete_text_view, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        UserModel model = allData.get(position);
        holder.setData(model);

        return convertView;
    }

    private class CustomHolder{

        TextView txvUserName, txvEmail;

        private void setId(View view){
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            txvEmail = (TextView)view.findViewById(R.id.txvEmail);
        }

        private void setData(UserModel model){
            txvUserName.setText(model.getUserName() + ", ");
            txvEmail.setText(model.getEmail() + ", ");
        }
    }
}
