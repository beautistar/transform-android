package com.sts.transform.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/14/2017.
 */

public class MacroPointModel implements Serializable {

    String bodyFat = "", bodyMass = "", protein = "", carb = "", fat = "", totalBlocks = "";
    String remainBlocks = "", eatBlocksTillNow = "";
    String startingBodyFatPercent = "";

    public String getStartingBodyFatPercent() {
        return startingBodyFatPercent;
    }

    public void setStartingBodyFatPercent(String startingBodyFatPercent) {
        this.startingBodyFatPercent = startingBodyFatPercent;
    }

    public String getEatBlocksTillNow() {
        return eatBlocksTillNow;
    }

    public void setEatBlocksTillNow(String eatBlocksTillNow) {
        this.eatBlocksTillNow = eatBlocksTillNow;
    }

    public String getRemainBlocks() {
        return remainBlocks;
    }

    public void setRemainBlocks(String remainBlocks) {
        this.remainBlocks = remainBlocks;
    }

    public String getBodyFat() {
        return bodyFat;
    }

    public void setBodyFat(String bodyFat) {
        this.bodyFat = bodyFat;
    }

    public String getBodyMass() {
        return bodyMass;
    }

    public void setBodyMass(String bodyMass) {
        this.bodyMass = bodyMass;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getCarb() {
        return carb;
    }

    public void setCarb(String carb) {
        this.carb = carb;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getTotalBlocks() {
        return totalBlocks;
    }

    public void setTotalBlocks(String totalBlocks) {
        this.totalBlocks = totalBlocks;
    }
}
