package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.GetLibraryDataRequest;
import com.sts.transform.API.JoinGroupRequest;
import com.sts.transform.Adapter.LibraryAdapter;
import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Main.LibraryDetailActivity;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.LibraryModel;
import com.sts.transform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class LibraryFragment extends BaseFragment {

    MainActivity activity;

    ListView lstLibrary;
    LibraryAdapter adapter;

    public LibraryFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_library, container, false);

        adapter = new LibraryAdapter(activity);
        lstLibrary = (ListView)fragment.findViewById(R.id.lstLibrary);
        lstLibrary.setAdapter(adapter);
        lstLibrary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String fileUrl = adapter.getItem(position).getFileUrl();
                Intent intent = new Intent(activity, LibraryDetailActivity.class);
                intent.putExtra(Constants.FILE_RUL, fileUrl);
                startActivity(intent);
            }
        });

        getAllData();

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    activity.closeProgress();
                    JSONObject response = new JSONObject(json);
                    int resultCode = response.getInt(ReqConst.RES_CODE);
                    if (resultCode == ReqConst.CODE_SUCCESS){

                        JSONArray jsonLibraryModelList =  response.getJSONArray(ReqConst.LIBRARY);
                        ArrayList<LibraryModel> mLibraryModelList = new ArrayList<>();

                        for (int i = 0; i < jsonLibraryModelList.length(); i++){
                            JSONObject jsonLibraryModel =  (JSONObject) jsonLibraryModelList.get(i);
                            LibraryModel mLibraryModel = new LibraryModel();

                            mLibraryModel.setId(jsonLibraryModel.getInt(ReqConst.ID));
                            mLibraryModel.setTitle(jsonLibraryModel.getString(ReqConst.TITLE));
                            mLibraryModel.setDescription(jsonLibraryModel.getString(ReqConst.DESCRIPTION));
                            mLibraryModel.setImageUrl(jsonLibraryModel.getString(ReqConst.PREVIEW_PICTURE));
                            mLibraryModel.setFileUrl(jsonLibraryModel.getString(ReqConst.FILE_URL));

                            mLibraryModelList.add(mLibraryModel);
                        }

                        adapter.refresh(mLibraryModelList);

                    }else {
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetLibraryDataRequest req = new GetLibraryDataRequest(res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }
}
