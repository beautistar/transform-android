package com.sts.transform.Commons;

/**
 * Created by STS on 11/7/2017.
 */

public class Constants {


    public static final String facebookURL = "https://www.facebook.com/TransformMacros/";
    public static final String instagramURL = "https://www.instagram.com/transform_macros/";


    public static int SPLASH_TIME = 2000;
    public static final int VOLLEY_TIME_OUT = 120000;
    public static final String SEDENTARY = "Sedentary";
    public static final float SEDENTARY_VALUE = 0.5f;

    public static final String LIGHT = "Walking";
    public static final float LIGHT_VALUE = 0.6f;

    public static final String MODERATE = "Moderate";
    public static final float MODERATE_VALUE = 0.7f;

    public static final String ACTIVE = "Active";
    public static final float ACTIVE_VALUE = 0.8f;

    public static final String VERY_ACTIVE = "Very Active";
    public static final float VERY_ACTIVE_VALUE = 0.9f;

    public static final String HEAVY_WEIGHT_TRAINING = "Heavy weight training";
    public static final float HEAVY_WEIGHT_TRAINING_VALUE = 1.0f;

    public static final String ID = "id";
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int MY_PEQUEST_CODE = 104;

    /// kinds of group
    public static final String PRIVATE = "private";
    public static final String PUBLIC = "public";

    /// group types
    public static final String GROUP_GENERAL_TYPE = "Health&Wellness";
    public static final String GROUP_MACRO_TYPE = "Macro Pro";
    public static final String GROUP_PALEO_TYPE = "Paleo";
    public static final String GROUP_KETO_TYPE = "Keto";

    public static final String NO_PROCESSED_FOOD = "param1";
    public static final String NOT_GOING = "param2";
    public static final String CONSUME = "param3";
    public static final String WOD = "param4";
    public static final String SLEEP = "param5";
    public static final String BENCHMARK = "param6";
    public static final String VARIOUS = "param7";
    public static final String FAT = "param8";
    public static final String FIST = "param9";
    public static final String VEGETABLES = "param10";
    public static final String FRUIT = "param11";
    public static final String PASTA = "param12";
    public static final String PALEO_BASE_POINT = "param13";
    public static final String KETO_BASE_POINT = "param14";
    public static final String GENERAL_BASE_POINT = "param15";


    public static final String MALE = "Male";
    public static final String FEMALE = "Female";

    public static final String pointContent1 = "+1 : No processed foods";
    public static final String pointContent2 = "+1 : Not going 5 hours without eating and not going more than 1/3 of daily blocks in single meal";
    public static final String pointContent3 = "+1 : Consume your body weight in grams of water(8oz=1glass)";
    public static final String pointContent4 = "+1 : 1hour of physical activity  oe 1 WOD";
    public static final String pointContent5 = "+1 : 7+hours of sleep";
    public static final String pointContent6 = "+5 : Include benchmark WOD for most improved, performed at beginning and end";
    public static final String pointContent7 = "Various amount:Allow challenge bets";
    public static final String pointContent8 = "+1 : 4 oz of low fat meat= 4 blocks of protein";
    public static final String pointContent9 = "+1 : 6 oz of fish= 4 blocks of protein";
    public static final String pointContent10 = "+1 : 2 cups of raw vegetables= 1 block of carbs";
    public static final String pointContent11 = "+1 : 1 piece of fruit= 2 blocks of carbs";
    public static final String pointContent12 = "+1 : 1 cup of cooked pasta, beans, rice=4 blocks of carbs";
    public static final String pointContent13 = "+5: Base points received for holding to Paleo restrictions for the day";
    public static final String pointContent14 = "+5: Base points received for holding to Keto restrictions for the day";
    public static final String pointContent15 = "+5: Base points received for Un-processed and clean eating restrictions";

    public static final String CHAT_MESSAGE = "chat";
    public static final String CHAT_MODEL = "chatModel";

    public static final String KEY_NEW_REGISTER = "newRegister";

    public static final int MIN_CARD_NUMBER_LENGTH = 13;
    public static final int MIN_YEAR_LENGTH = 2;
    public static final int MIN_CVV_LENGTH = 3;
    public static final String YEAR_PREFIX = "20";


    public static final String BENCHMARK_POINT = "com.sts.transform.benchmark";

    public static final String HOME_FRAGMENT = "HomeFragment";
    public static final String CREATE_GROUP_FRAGMENT = "CreateGroupFragment";
    public static final String EDIT_PROFILE_FRAGMENT = "EditProfileFragment";
    public static final String HOW_FRAGMENT = "HowFragment";
    public static final String INVITED_GROUP_INFO_FRAGMENT = "InviteGroupFragment";
    public static final String JOIN_GROUP_FRAGMENT = "JoinGroupFragment";
    public static final String LIBRARY_FRAGMENT = "LibraryFragment";
    public static final String MANAGE_ACCOUNT_FRAGMENT = "ManageAccountFragment";
    public static final String PROFILE_FRAGMENT = "ProfileFragment";
    public static final String FILE_RUL =  "fileUrl";
}
