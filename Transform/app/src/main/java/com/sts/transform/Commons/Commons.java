package com.sts.transform.Commons;

import android.widget.ProgressBar;

import com.sts.transform.Main.MainActivity;
import com.sts.transform.Models.GroupModel;
import com.sts.transform.Models.RankingModel;
import com.sts.transform.Models.UserModel;

import java.net.PortUnreachableException;
import java.util.ArrayList;

/**
 * Created by STS on 11/13/2017.
 */

public class Commons {

    public static UserModel user;
    public static String token = "";
    public static boolean isAppRunning = false;
    public static GroupModel invitedGroupInfo;
    public static int groupId = 0;
    public static String groupName = "";
    public static String message = "";
    public static boolean isChatFragment = false;
    public static String[][] bodyFatPercentTableForMale = new String [38][58];
    public static String[][] abcTableForFemale = new String [62][6];
    public static ArrayList<String> heightList = new ArrayList<>();
    public static ArrayList<String> weightList = new ArrayList<>();
    public static ArrayList<String> hipList = new ArrayList<>();
    public static ArrayList<String> abdomenList = new ArrayList<>();
    public static ArrayList<String > wristList = new ArrayList<>();
    public static ArrayList<String > waistList = new ArrayList<>();
    public static ArrayList<RankingModel> rankingModelList =  new ArrayList<>();
    public static ArrayList<String> pointList = new ArrayList<>();
    public static ArrayList<GroupModel> prevChallengeList = new ArrayList<>();
    public static String currentFragment = "";


}
