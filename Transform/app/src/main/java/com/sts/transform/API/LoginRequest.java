package com.sts.transform.API;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.transform.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class LoginRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_LOGIN;
    private Map<String,String> params;

    public LoginRequest(String email, String password , Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.EMAIL, email);
        params.put(ReqConst.PASSWORD, password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
