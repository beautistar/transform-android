package com.sts.transform.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by STS on 11/13/2017.
 */

public class UserModel implements Serializable {

    int id = 0, benchmark = 0, totalPointOfSelectedGroup = 0;
    String userName = "", email = "", password = "", photoUrl = "";
    UserPropertyModel userPropertyModel;
    MacroPointModel macroPointModel =  new MacroPointModel();
    ArrayList<PointModel> pointModels = new ArrayList<>();

    public int getTotalPointOfSelectedGroup() {
        return totalPointOfSelectedGroup;
    }

    public void setTotalPointOfSelectedGroup(int totalPointOfSelectedGroup) {
        this.totalPointOfSelectedGroup = totalPointOfSelectedGroup;
    }

    public int getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(int benchmark) {
        this.benchmark = benchmark;
    }

    public ArrayList<PointModel> getPointModels() {
        return pointModels;
    }

    public void setPointModels(ArrayList<PointModel> pointModels) {
        this.pointModels = pointModels;
    }

    public MacroPointModel getMacroPointModel() {
        return macroPointModel;
    }

    public void setMacroPointModel(MacroPointModel macroPointModel) {
        this.macroPointModel = macroPointModel;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public UserPropertyModel getUserPropertyModel() {
        return userPropertyModel;
    }

    public void setUserPropertyModel(UserPropertyModel userPropertyModel) {
        this.userPropertyModel = userPropertyModel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
