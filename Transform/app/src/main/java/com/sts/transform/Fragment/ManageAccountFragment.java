package com.sts.transform.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sts.transform.Base.BaseFragment;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.R;

/**
 * Created by STS on 4/30/2017.
 */

@SuppressLint("ValidFragment")
public class ManageAccountFragment extends BaseFragment {

    MainActivity activity;

    public ManageAccountFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.fragment_manage_account, container, false);
        return fragment;
    }
}
