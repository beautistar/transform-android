package com.sts.transform.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.transform.API.AddPointRequest;
import com.sts.transform.API.DeletePointRequest;
import com.sts.transform.Commons.Commons;
import com.sts.transform.Commons.Constants;
import com.sts.transform.Commons.ReqConst;
import com.sts.transform.Fragment.HomeFragment;
import com.sts.transform.Main.MainActivity;
import com.sts.transform.Preference.PrefConst;
import com.sts.transform.Preference.Preference;
import com.sts.transform.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by STS on 11/26/2017.
 */

public class PointParameterInHomeFragmentAdapter extends BaseAdapter {

    MainActivity activity;
    HomeFragment mHomeFragment;
    public CheckBox chkBenchmark, chkBet;
    public int benchmarkPosition =  -1, betPosition = -1;
    boolean hasBenchmark = false;

    ArrayList<String> allData = new ArrayList<>();

    public PointParameterInHomeFragmentAdapter(MainActivity activity, HomeFragment homeFragment) {
        this.activity = activity;
        this.mHomeFragment =  homeFragment;
        hasBenchmark = false;
    }

    public void refresh(ArrayList<String> data){
        allData.clear();
        allData.addAll(data);

        hasBenchmark = false;
        benchmarkPosition = -1;
        notifyDataSetChanged();
    }

    public void hasBenchmark(){
        this.hasBenchmark = true;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public Object getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView ==  null){

            holder = new CustomHolder();

            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_point_parameter, parent, false);

            holder.setId(convertView);

            String pointContent = allData.get(position);
            holder.setData(pointContent, position);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{
        CheckBox chPoint;
        String pointContent = "";

        private void setId(View view){
            chPoint = (CheckBox)view.findViewById(R.id.chPoint);
            chPoint.setOnClickListener(this);
            chPoint.setChecked(false);
        }

        private void setData(String point, int position){

            switch (point){
                case Constants.NO_PROCESSED_FOOD:
                    pointContent = Constants.pointContent1;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT1, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.NOT_GOING:
                    pointContent = Constants.pointContent2;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT2, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.CONSUME:
                    pointContent = Constants.pointContent3;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT3, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.WOD:
                    pointContent = Constants.pointContent4;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT4, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.SLEEP:
                    pointContent = Constants.pointContent5;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT5, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.BENCHMARK:
                    benchmarkPosition = position;
                    if (hasBenchmark) chPoint.setChecked(true);
                    pointContent = Constants.pointContent6;
                    break;
                case Constants.VARIOUS:
                    betPosition = position;
                    pointContent = Constants.pointContent7;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT7, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.FAT:
                    pointContent = Constants.pointContent8;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT8, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.FIST:
                    pointContent = Constants.pointContent9;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT9, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.VEGETABLES:
                    pointContent = Constants.pointContent10;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT10, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.FRUIT:
                    pointContent = Constants.pointContent11;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT11, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.PASTA:
                    pointContent = Constants.pointContent12;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT12, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.PALEO_BASE_POINT:
                    pointContent = Constants.pointContent13;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT13, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.KETO_BASE_POINT:
                    pointContent = Constants.pointContent14;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT14, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
                case Constants.GENERAL_BASE_POINT:
                    pointContent = Constants.pointContent15;
                    if (Preference.getInstance().getValue(activity, PrefConst.POINT15, "").equals("1")){
                        chPoint.setChecked(true);
                    }
                    break;
            }

            chPoint.setText(pointContent);
        }

        private void addPoint(final String checkedPoint) {
            final Response.Listener<String> res = new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {
                    activity.closeProgress();
                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){
                            mHomeFragment.todayPoints ++;
                            mHomeFragment.totalPoints ++;

                            mHomeFragment.txvTodayPoints.setText(String.valueOf(mHomeFragment.todayPoints));
                            mHomeFragment.txvTotalPoints.setText(String.valueOf(mHomeFragment.totalPoints));

                            for (int i = 0; i < Commons.user.getPointModels().size(); i++){
                                if (Commons.user.getPointModels().get(i).getGroupId() == mHomeFragment.selectedGroupId){
                                    Commons.user.getPointModels().get(i).setTotalPoints(mHomeFragment.totalPoints);
                                    Commons.user.getPointModels().get(i).setTodayPoints(mHomeFragment.todayPoints);
                                }
                            }

                            saveCheckedPoint(checkedPoint);

                            activity.refreshUserInfo();

                        }else {
                            activity.showAlertDialog(activity.getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        activity.closeProgress();
                        activity.showAlertDialog(activity.getString(R.string.server_failed));
                    }
                }
            };
            final Response.ErrorListener error = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(activity.getString(R.string.server_failed));
                }
            };

            activity.showProgress();
            AddPointRequest req = new AddPointRequest(String.valueOf(Commons.user.getId()), String.valueOf(mHomeFragment.selectedGroup.getId()), String.valueOf(1), res, error);
            req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue request = Volley.newRequestQueue(activity);
            request.add(req);
        }

        private void deletePoint(String pointCnt, final String checkedPoint) {
            final Response.Listener<String> res = new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {
                    activity.closeProgress();
                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){
                            mHomeFragment.todayPoints --;
                            mHomeFragment.totalPoints --;

                            mHomeFragment.txvTodayPoints.setText(String.valueOf(mHomeFragment.todayPoints));
                            mHomeFragment.txvTotalPoints.setText(String.valueOf(mHomeFragment.totalPoints));

                            for (int i = 0; i < Commons.user.getPointModels().size(); i++){
                                if (Commons.user.getPointModels().get(i).getGroupId() == mHomeFragment.selectedGroupId){
                                    Commons.user.getPointModels().get(i).setTotalPoints(mHomeFragment.totalPoints);
                                    Commons.user.getPointModels().get(i).setTodayPoints(mHomeFragment.todayPoints);
                                }
                            }

                            removeCheckedPoint(checkedPoint);

                            activity.refreshUserInfo();

                        }else {
                            activity.showAlertDialog(activity.getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        activity.closeProgress();
                        activity.showAlertDialog(activity.getString(R.string.server_failed));
                    }
                }
            };
            final Response.ErrorListener error = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(activity.getString(R.string.server_failed));
                }
            };

            activity.showProgress();
            DeletePointRequest req = new DeletePointRequest(String.valueOf(Commons.user.getId()), String.valueOf(mHomeFragment.selectedGroup.getId()), pointCnt, res, error);
            req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue request = Volley.newRequestQueue(activity);
            request.add(req);
        }

        private void deletePoint5(String pointCnt, final String checkedPoint) {
            final Response.Listener<String> res = new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {
                    activity.closeProgress();
                    try{
                        JSONObject response = new JSONObject(json);
                        int resultCode = response.getInt(ReqConst.RES_CODE);
                        if (resultCode == ReqConst.CODE_SUCCESS){
                            mHomeFragment.todayPoints = mHomeFragment.todayPoints - 5;
                            mHomeFragment.totalPoints = mHomeFragment.totalPoints - 5;

                            mHomeFragment.txvTodayPoints.setText(String.valueOf(mHomeFragment.todayPoints));
                            mHomeFragment.txvTotalPoints.setText(String.valueOf(mHomeFragment.totalPoints));

                            for (int i = 0; i < Commons.user.getPointModels().size(); i++){
                                if (Commons.user.getPointModels().get(i).getGroupId() == mHomeFragment.selectedGroupId){
                                    Commons.user.getPointModels().get(i).setTotalPoints(mHomeFragment.totalPoints);
                                    Commons.user.getPointModels().get(i).setTodayPoints(mHomeFragment.todayPoints);
                                }
                            }

                            removeCheckedPoint(checkedPoint);

                            activity.refreshUserInfo();

                        }else {
                            activity.showAlertDialog(activity.getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        activity.closeProgress();
                        activity.showAlertDialog(activity.getString(R.string.server_failed));
                    }
                }
            };
            final Response.ErrorListener error = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(activity.getString(R.string.server_failed));
                }
            };

            activity.showProgress();
            DeletePointRequest req = new DeletePointRequest(String.valueOf(Commons.user.getId()), String.valueOf(mHomeFragment.selectedGroup.getId()), pointCnt, res, error);
            req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue request = Volley.newRequestQueue(activity);
            request.add(req);
        }

        private void saveCheckedPoint(String checkedPoint) {
            switch (checkedPoint){
                case Constants.pointContent1:
                    Preference.getInstance().put(activity, PrefConst.POINT1, "1");
                    break;
                case Constants.pointContent2:
                    Preference.getInstance().put(activity, PrefConst.POINT2, "1");
                    break;
                case Constants.pointContent3:
                    Preference.getInstance().put(activity, PrefConst.POINT3, "1");
                    break;
                case Constants.pointContent4:
                    Preference.getInstance().put(activity, PrefConst.POINT4, "1");
                    break;
                case Constants.pointContent5:
                    Preference.getInstance().put(activity, PrefConst.POINT5, "1");
                    break;
                case Constants.pointContent6:
                    Preference.getInstance().put(activity, PrefConst.POINT6, "1");
                    break;
                case Constants.pointContent7:
                    Preference.getInstance().put(activity, PrefConst.POINT7, "1");
                    break;
                case Constants.pointContent8:
                    Preference.getInstance().put(activity, PrefConst.POINT8, "1");
                    break;
                case Constants.pointContent9:
                    Preference.getInstance().put(activity, PrefConst.POINT9, "1");
                    break;
                case Constants.pointContent10:
                    Preference.getInstance().put(activity, PrefConst.POINT10, "1");
                    break;
                case Constants.pointContent11:
                    Preference.getInstance().put(activity, PrefConst.POINT11, "1");
                    break;
                case Constants.pointContent12:
                    Preference.getInstance().put(activity, PrefConst.POINT12, "1");
                    break;
                case Constants.pointContent13:
                    Preference.getInstance().put(activity, PrefConst.POINT13, "1");
                    break;
                case Constants.pointContent14:
                    Preference.getInstance().put(activity, PrefConst.POINT14, "1");
                    break;
                case Constants.pointContent15:
                    Preference.getInstance().put(activity, PrefConst.POINT15, "1");
                    break;
            }

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String dd = df.format(c);

            dd = (dd.split("-"))[0];

            Preference.getInstance().put(activity, PrefConst.DATE_FOR_POINT, dd);
        }

        private void removeCheckedPoint(String checkedPoint) {
            switch (checkedPoint){
                case Constants.pointContent1:
                    Preference.getInstance().put(activity, PrefConst.POINT1, "0");
                    break;
                case Constants.pointContent2:
                    Preference.getInstance().put(activity, PrefConst.POINT2, "0");
                    break;
                case Constants.pointContent3:
                    Preference.getInstance().put(activity, PrefConst.POINT3, "0");
                    break;
                case Constants.pointContent4:
                    Preference.getInstance().put(activity, PrefConst.POINT4, "0");
                    break;
                case Constants.pointContent5:
                    Preference.getInstance().put(activity, PrefConst.POINT5, "0");
                    break;
                case Constants.pointContent6:
                    Preference.getInstance().put(activity, PrefConst.POINT6, "0");
                    break;
                case Constants.pointContent7:
                    Preference.getInstance().put(activity, PrefConst.POINT7, "0");
                    break;
                case Constants.pointContent8:
                    Preference.getInstance().put(activity, PrefConst.POINT8, "0");
                    break;
                case Constants.pointContent9:
                    Preference.getInstance().put(activity, PrefConst.POINT9, "0");
                    break;
                case Constants.pointContent10:
                    Preference.getInstance().put(activity, PrefConst.POINT10, "0");
                    break;
                case Constants.pointContent11:
                    Preference.getInstance().put(activity, PrefConst.POINT11, "0");
                    break;
                case Constants.pointContent12:
                    Preference.getInstance().put(activity, PrefConst.POINT12, "0");
                    break;
                case Constants.pointContent13:
                    Preference.getInstance().put(activity, PrefConst.POINT13, "0");
                    break;
                case Constants.pointContent14:
                    Preference.getInstance().put(activity, PrefConst.POINT14, "0");
                    break;
                case Constants.pointContent15:
                    Preference.getInstance().put(activity, PrefConst.POINT15, "0");
                    break;
            }

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String dd = df.format(c);

            dd = (dd.split("-"))[0];

            Preference.getInstance().put(activity, PrefConst.DATE_FOR_POINT, dd);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.chPoint:
                    if (chPoint.getText().equals(Constants.pointContent6)){
                        if (chPoint.isChecked()){
                            chPoint.setChecked(false);
                        }else {
                            chPoint.setChecked(true);
                        }
                        return;
                    }else {

                        if (mHomeFragment.selectedGroup.getChallengeFinished().equals("1")) return;

                        if (chPoint.isChecked()){
                            if (chPoint.getText().toString().contains("+5: Base")){
                                mHomeFragment.addPoint5(chPoint.getText().toString().trim());
                            }else {
                                addPoint(chPoint.getText().toString().trim());
                            }

                        }else {
                            if (mHomeFragment.todayPoints == 0) return;
                            if (chPoint.getText().toString().contains("+5: Base")){
                                deletePoint5("5", chPoint.getText().toString().trim());
                            }else {
                                deletePoint("1", chPoint.getText().toString().trim());
                            }
                        }
                    }
                    break;
            }
        }
    }
}
